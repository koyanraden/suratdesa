<?php

class Front
{
    private $CI;
    private $value = array();

    public function __construct()
    {
        $this->CI = get_instance();
    }

    public function load($view = '', $data = array(), $returning = false)
    {
        $this->value['value'] = $this->CI->load->view($view, $data, true);
        return $this->CI->load->view('template_front', $this->value, $returning);
    }
}
