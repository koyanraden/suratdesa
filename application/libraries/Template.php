<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Template
{
    private $CI;
    public $view_data = array();

    function __construct()
    {
        $this->CI = get_instance();
    }

    function load($template = '', $view = '', $data = array(), $return = false)
    {
        $this->view_data['value'] = $this->CI->load->view($view, $data, true);
        return $this->CI->load->view($template, $this->view_data, $return);
    }
}
