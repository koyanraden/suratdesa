<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_users', 'users');
    }

    public function index()
    {
        $data['title'] = 'Login';
        $this->template->load('template_auth', 'auth/login', $data);

        if ($_POST) {
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $this->login($username, $password);
        }
    }

    public function registrasi()
    {
        $data['title'] = 'Registrasi';
        $this->template->load('template_auth', 'auth/registration', $data);

        if ($_POST) {
            $nik = $this->input->post('nik');
            $username = $this->input->post('username');
            $email = $this->input->post('email');
            $telp = $this->input->post('telp');
            $password1 = $this->input->post('password1');
            $password2 = $this->input->post('password2');

            $penduduk = $this->db->get_where('penduduk', ['nik' => $nik])->row_array();

            if ($penduduk) {
                if ($password1 == $password2) {
                    $a_data = [
                        'nama' => $penduduk['namalengkap'],
                        'nik' => $nik,
                        'username' => $username,
                        'email' => $email,
                        'notelp' => $telp,
                        'password' => password_hash($password1, PASSWORD_DEFAULT),
                        'idrole' => 3
                    ];
                    $ok = $this->users->insert($a_data);
                    $ok ? showMessage('Berhasil mendaftar akun', 'success') : showMessage('Gagal mendaftar akun, coba lagi!', 'danger');
                    redirect('auth');
                } else {
                    showMessage('Password tidak cocok!', 'danger');
                    redirect('auth/registrasi');
                }
            } else {
                showMessage('NIK Tidak terdaftar', 'danger');
                redirect('auth/registrasi');
            }
        }
    }

    public function login($username, $pass)
    {
        $user = $this->users->getBy(['username' => $username])->row_array();

        if ($user) {
            if (password_verify($pass, $user['password'])) {
                $data = [
                    'nama' => $user['nama'],
                    'username' => $user['username'],
                    'email' => $user['email'],
                    'nik' => $user['nik'],
                    'idrole' => $user['idrole']
                ];
                $this->session->set_userdata($data);
                if ($user['idrole'] < 3) {
                    redirect('home');
                } else {
                    redirect('beranda');
                }
            } else {
                showMessage('Password salah!', 'danger');
                redirect('auth');
            }
        } else {
            showMessage('User tidak terdaftar!', 'danger');
            redirect('auth');
        }
    }

    public function logout()
    {
        $data = [
            'nama',
            'username',
            'email',
            'nik',
            'idrole'
        ];
        $this->session->unset_userdata($data);
        redirect('auth');
    }
}
