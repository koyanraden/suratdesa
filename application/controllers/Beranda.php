<?php

class Beranda extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['title'] = 'Beranda';
        $data['nav'] = 'beranda';
        $a_data = [
            'penduduk' => $this->db->get('penduduk')->num_rows(),
            'keluarga' => $this->db->get('kartukeluarga')->num_rows(),
            'jenis_surat' => $this->db->get('jenissurat')->num_rows()
        ];
        $data['a_data'] = $a_data;
        $data['a_jenis'] = $this->db->get('jenissurat')->result_array();
        $this->front->load('client/home/index', $data);
    }
}
