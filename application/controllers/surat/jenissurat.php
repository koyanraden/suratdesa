<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jenissurat extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }


        //set default
        $this->title = 'Jenis Surat';
        $this->menu = 'jenissurat';
        $this->parent = 'surat';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {

        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'jenissurat', 'label' => 'Jenis Surat'];
        $a_kolom[] = ['kolom' => 'persyaratan', 'label' => 'Persyaratan', 'type' => 'A', 'is_null' => true, 'is_tiny' => 'yes'];

        $this->a_kolom = $a_kolom;
    }
}
