<?php

use Mpdf\Mpdf;

defined('BASEPATH') or exit('No direct script access allowed');

class Pengajuan extends MY_Controller
{
    public $PDF;

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }
        $this->load->model('M_cetaksurat', 'cetaksurat');
        $this->PDF = new Mpdf();
    }

    public function index()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', base_url());
        $this->breadcrumb->append_crumb('Pengajuan', base_url('surat/pengajuan'));

        $data['title'] = 'Pengajuan Surat';
        $data['user'] = $this->user;
        $query = "SELECT * FROM cetaksurat cs JOIN jenissurat js ON cs.idjenissurat=js.idjenissurat JOIN penduduk p ON cs.nik=p.nik JOIN bukti b ON cs.idbukti=b.idbukti";
        $a_data = $this->db->query($query)->result_array();
        $data['pengajuan'] = $a_data;
        $this->template->load('template', 'pengajuan/index', $data);
    }

    public function detail($id = null)
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', base_url());
        $this->breadcrumb->append_crumb('Pengajuan', base_url('surat/pengajuan'));
        $this->breadcrumb->append_crumb('Detail Pengajuan', '#');

        $data['title'] = 'Detail Pengajuan';
        $a_detail = $this->cetaksurat->getReffById($id)->row_array();
        if (!empty($a_detail['penduduk_meninggal'])) {
            $penduduk_meninggal = $this->db->get_where('penduduk', ['nik' => $a_detail['penduduk_meninggal']])->row_array();
        }
        $a_kolom = array();
        $a_kolom[] = ['kolom' => 'ID Cetak', 'value' => $a_detail['idcetak']];
        $a_kolom[] = ['kolom' => 'Nama Pengaju', 'value' => $a_detail['namalengkap']];
        $a_kolom[] = ['kolom' => 'TTL', 'value' => $a_detail['tempatlahir'] . ', ' . $a_detail['tgl_lahir']];
        $a_kolom[] = ['kolom' => 'Agama', 'value' => $a_detail['agama']];
        $a_kolom[] = ['kolom' => 'Pendidikan Terakhir', 'value' => $a_detail['pendidikan']];
        $a_kolom[] = ['kolom' => 'Pekerjaan', 'value' => $a_detail['jenispekerjaan']];
        $a_kolom[] = ['kolom' => 'Status Kawin', 'value' => $a_detail['is_kawin'] == 1 ? 'Sudah Kawin' : 'Belum Kawin'];
        $a_kolom[] = ['kolom' => 'Kewarganegaraan', 'value' => $a_detail['kewarganegaraan']];

        $a_surat = array();
        $a_surat[] = ['kolom' => 'No Surat', 'value' => $a_detail['nosurat'] != null ? $a_detail['nosurat'] : 'Belum Ditentukan'];
        $a_surat[] = ['kolom' => 'Jenis Surat', 'value' => $a_detail['jenissurat']];
        $a_surat[] = ['kolom' => 'Tgl Pengajuan', 'value' => $a_detail['request_at']];
        $a_surat[] = ['kolom' => 'Bukti', 'value' => $a_detail['bukti'], 'type' => 'F', 'path' => base_url('assets/bukti/')];
        $a_surat[] = ['kolom' => 'Status', 'value' => $a_detail['status'] == 0 ? 'Belum Dicetak' : 'Sudah Dicetak'];
        $a_surat[] = ['kolom' => 'Alasan', 'value' => $a_detail['alasan'], 'justify' => 'center'];

        $data['penduduk_meninggal'] = $penduduk_meninggal;
        $data['a_kolom'] = $a_kolom;
        $data['a_surat'] = $a_surat;
        $data['a_data'] = $a_detail;
        $this->template->load('template', 'pengajuan/detail', $data);

        if (!empty($_POST)) {
            $nosurat = $this->input->post('nosurat');
            $idcetak = $this->input->post('idcetak');
            $a_data = $this->cetaksurat->getReffById($idcetak)->row_array();
            if (!empty($a_data['penduduk_meninggal'])) {
                $penduduk_meninggal = $this->db->get_where('penduduk', ['nik' => $a_data['penduduk_meninggal']])->row_array();
                $data['penduduk_meninggal'] = $penduduk_meninggal;
            }

            if ($a_data['idjenissurat'] == 4) {
                $kel = $this->db->get_where('list_keluarga', ['nik' => $a_data['nik']])->row_array();
                $nokk = $kel['nokk'];
                $query = "SELECT * FROM list_keluarga JOIN penduduk USING(nik) WHERE nokk='$nokk'";
                $keluarga = $this->db->query($query)->result_array();
                $data['keluarga'] = $keluarga;
            }

            $data['nosurat'] = $nosurat;
            $data['a_data'] = $a_data;

            switch (strval($a_data['idjenissurat'])) {
                case '1':
                    $this->PDF->WriteHTML($this->load->view('laporan/repp_kematian', $data, true));
                    $this->PDF->Output('surat_ket_kematian_' . $nosurat . '.pdf', 'D');
                    break;
                case '2':
                    $this->PDF->WriteHTML($this->load->view('laporan/repp_usaha', $data, true));
                    $this->PDF->Output('surat_ket_usaha_' . $nosurat . '.pdf', 'D');
                    break;
                case '3':
                    $this->PDF->WriteHTML($this->load->view('laporan/repp_domisili', $data, true));
                    $this->PDF->Output('surat_ket_domisili_' . $nosurat . '.pdf', 'D');
                    break;
                case '4':
                    $this->PDF->WriteHTML($this->load->view('laporan/repp_pindah', $data, true));
                    $this->PDF->Output('surat_ket_pindah_' . $nosurat . '.pdf', 'D');
                    break;
                case '5':
                    $this->PDF->WriteHTML($this->load->view('laporan/repp_menikah', $data, true));
                    $this->PDF->Output('surat_ket_menikah_' . $nosurat . '.pdf', 'D');
                    break;
            }

            $a_update = [
                'nosurat' => $nosurat,
                'status' => 1,
                'cetak_by' => $this->user['iduser'],
                'cetak_at' => date('d-M-Y H:i:s')
            ];

            $ok = $this->db->update('cetaksurat', $a_update, ['idcetak' => $idcetak]);
            $ok ? showMessage('Berhasil mencetak surat', 'success') : showMessage('Gagal mencetak surat', 'danger');
            redirect('surat/pengajuan');
        }
    }

    public function tolak($id)
    {
        $ok = $this->db->update('cetaksurat', ['status' => 2], ['idcetak' => $id]);
        $ok ? showMessage('Berhasil menolak surat!', 'success') : showMessage('Gagal menolak surat', 'danger');
        redirect('surat/pengajuan');
    }
}
