<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profil extends MY_Controller
{
    public $user;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_users', 'users');
        $this->user = $this->users->getBy(['username' => $this->session->userdata('username')])->row_array();
    }

    public function index()
    {
        $data['title'] = 'Profil';
        $data['nav'] = 'Home';
        $data['users'] = $this->user;
        $data['active'] = 'profil';
        $this->load->view('client/inc/header', $data);
        $this->load->view('client/inc/navbar', $data);
        $this->load->view('client/profil/inc_profilheader', $data);
        $this->load->view('client/profil/index', $data);
        $this->load->view('client/inc/footer', $data);
    }

    public function edit()
    {
        $data['title'] = 'Edit Profil';
        $data['nav'] = 'Home';
        $data['users'] = $this->user;
        $data['active'] = 'edit';
        $this->load->view('client/inc/header', $data);
        $this->load->view('client/inc/navbar', $data);
        $this->load->view('client/profil/inc_profilheader', $data);
        $this->load->view('client/profil/edit', $data);
        $this->load->view('client/inc/footer', $data);

        if ($_POST) {
            $nama = $this->input->post('nama');
            $username = $this->input->post('username');
            $email = $this->input->post('email');

            if ($this->session->userdata('username') != $username) {
                $cekUsername = $this->db->get_where('users', ['username' => $username]);
                if ($cekUsername->num_rows() < 1) {
                    $a_data = [
                        'username' => $username,
                        'nama' => $nama,
                        'email' => $email
                    ];
                    $ok = $this->db->update('users', $a_data, ['username' => $this->session->userdata('username')]);

                    if ($ok) {
                        showMessage('Berhasil merubah data!', 'success');
                        $unset = [
                            'username',
                            'email',
                            'nama',
                        ];
                        $this->session->unset_userdata($unset);
                        $set = [
                            'username' => $username,
                            'email' => $email,
                            'nama' => $nama
                        ];
                        $this->session->set_userdata($set);
                    } else {
                        showMessage('Gagal merubah data, coba lagi!', 'danger');
                    }
                    redirect('profil/edit');
                } else {
                    showMessage('Username telah digunakan', 'danger');
                    redirect('profil/edit');
                }
            } else {
                $sess_nama = $this->session->userdata('nama');
                $sess_email = $this->session->userdata('email');

                if ($sess_nama != $nama || $sess_email != $email) {
                    $data = [
                        'nama' => $nama,
                        'email' => $email
                    ];
                    $ok = $this->db->update('users', $data, ['username' => $this->session->userdata('username')]);

                    if ($ok) {
                        $unset = [
                            'email',
                            'nama',
                        ];
                        $this->session->unset_userdata($unset);
                        $set = [
                            'email' => $email,
                            'nama' => $nama
                        ];
                        $this->session->set_userdata($set);
                        showMessage('Berhasil merubah data!', 'success');
                    } else {
                        showMessage('Gagal mengubah data, coba lagi!', 'danger');
                    }

                    redirect('profil/edit');
                }
            }
        }
    }

    public function password()
    {
        $data['title'] = 'Ganti Password';
        $data['nav'] = 'Home';
        $data['users'] = $this->user;
        $data['active'] = 'password';
        $this->load->view('client/inc/header', $data);
        $this->load->view('client/inc/navbar', $data);
        $this->load->view('client/profil/inc_profilheader', $data);
        $this->load->view('client/profil/password', $data);
        $this->load->view('client/inc/footer', $data);

        if ($_POST) {
            $oldpass = $this->input->post('oldpassword');
            $newpass = $this->input->post('newpassword');
            $newpassconf = $this->input->post('newpasswordconf');

            if (password_verify($oldpass, $this->user['password'])) {
                if ($newpass == $newpassconf) {
                    if ($newpass != $oldpass) {
                        $update = $this->users->update(['password' => password_hash($newpass, PASSWORD_DEFAULT)], $this->user['iduser']);
                        $update ? showMessage('Berhasil merubah password', 'success') : showMessage('Gagal merubah password!', 'danger');
                    } else {
                        showMessage('Password tidak boleh sama dengan yang lama!', 'danger');
                    }
                } else {
                    showMessage('Password tidak cocok!', 'danger');
                }
            } else {
                showMessage('Password salah!', 'danger');
            }

            redirect('profil/password');
        }
    }
}
