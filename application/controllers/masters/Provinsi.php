<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Provinsi extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_provinsi', 'provinsi');


        //set default
        $this->title = 'Data Provinsi';
        $this->menu = 'provinsi';
        $this->parent = 'masters';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {
        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'provinsi', 'label' => 'Provinsi'];

        $this->a_kolom = $a_kolom;
    }
}
