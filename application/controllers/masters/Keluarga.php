<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Keluarga extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_kartukeluarga', 'kartukeluarga');
        $this->load->model('M_listKeluarga', 'listKeluarga');
    }

    public function index()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url());
        $this->breadcrumb->append_crumb('Data Keluarga', site_url('masters/keluarga'));

        $data['title'] = 'Data Keluarga';
        $data['menu_now'] = 'masters';
        $data['user'] = $this->user;
        $data['keluarga'] = $this->kartukeluarga->getReff()->result_array();
        $this->template->load('template', 'keluarga/index', $data);
    }

    public function formKeluarga($id = 'tambah')
    {
        if ($id == 'tambah') {
            $title = 'Tambah Keluarga';
            $key = '';
            $data['keluarga'] = $key;
            $data['action'] = 'simpan';
        } else {
            $title = 'Edit Keluarga';
            $data['keluarga'] = $this->kartukeluarga->getBy(['nokk' => $id])->row_array();
            $data['keluarga'] = $data['keluarga'];
            $data['action'] = 'edit';
        }

        $data['title'] = $title;
        $data['profile'] = $title;
        $data['menu_now'] = 'Keluarga';
        $data['desa'] = $this->db->get('desa')->result_array();
        $data['rtrw'] = $this->db->get('rtrw')->result_array();
        $data['user'] = $this->user;
        $this->template->load('template', 'keluarga/formkeluarga', $data);

        if (!empty($_POST)) {
            $nokk = $this->input->post('nokk');
            $alamat = $this->input->post('alamat');
            $kodepos = $this->input->post('kodepos');
            $desa = $this->input->post('desa');
            $rtrw = $this->input->post('rtrw');
            $act = $this->input->post('act');
            $this_key = $this->input->post('key');

            $a_data = [
                'nokk' => $nokk,
                'alamat' => $alamat,
                'kodepos' => $kodepos,
                'iddesa' => $desa,
                'idrtrw' => $rtrw
            ];

            if (!empty($nokk) && !empty($alamat) && !empty($kodepos) && !empty($desa) && !empty($rtrw)) {
                switch ($act) {
                    case 'simpan':
                        $ok = $this->kartukeluarga->insert($a_data);
                        $ok ? showMessage('Berhasil menambahkan keluarga baru', 'success') : showMessage('Gagal menambahkan data, Coba lagi!', 'danger');
                        break;
                    case 'edit':
                        $ok = $this->kartukeluarga->update($a_data, $this_key);
                        $ok ? showMessage('Berhasil merubah keluarga baru', 'success') : showMessage('Gagal merubah data, Coba lagi!', 'danger');
                        break;
                }
            } else {
                showMessage('Mohon lengkapi data!', 'danger');
            }

            redirect('masters/keluarga');
        }
    }

    public function deleteKeluarga($id)
    {
        $ok = $this->kartukeluarga->delete($id);
        $ok ? showMessage('Berhasil menghapus data!', 'success') : showMessage('Gagal menghapus data!', 'danger');
        redirect('masters/keluarga');
    }

    public function detailKeluarga($id = null)
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url());
        $this->breadcrumb->append_crumb('Data Keluarga', site_url('masters/keluarga'));
        $this->breadcrumb->append_crumb('Detail Keluarga', '#');

        $data['title'] = 'Detail Keluarga';
        $data['profile'] = 'Detail Keluarga';
        $data['menu_now'] = 'Masters';
        $data['list_keluarga'] = $this->listKeluarga->getReff($id)->result_array();
        $data['jumlahKel'] = $this->listKeluarga->getReff($id)->num_rows();
        $data['kepalakel'] = $this->listKeluarga->getReff($id, 'Kepala Keluarga')->row_array();
        $data['user'] = $this->user;
        $data['nokartukeluarga'] = $id;
        $this->template->load('template', 'keluarga/detailkeluarga', $data);
    }

    public function detailForm()
    {
        if (!empty($_POST)) {
            $nik = $this->input->post('penduduk');
            $status_hub = $this->input->post('status_hub');
            $status_kawin = $this->input->post('status_kawin');
            $nokk = $this->input->post('nokk');

            $a_insert = [
                'nokk' => $nokk,
                'nik' => $nik
            ];

            $a_update = [
                'status_hub' => $status_hub,
                'is_kawin' => $status_kawin
            ];

            if ($this->db->get_where('list_keluarga', ['nokk' => $nokk, 'nik' => $nik])->num_rows() < 1) {
                $up = $this->db->update('penduduk', $a_update, ['nik' => $nik]);
                $ins = $this->db->insert('list_keluarga', $a_insert);

                $up && $ins ? showMessage('Berhasil menambahkan anggota keluarga baru!', 'success') : showMessage('Gagal menambahkan anggota keluarga, Coba lagi!', 'danger');
            } else {
                showMessage('Data sudah terdaftar, Coba cek terlebih dahulu!', 'danger');
            }

            redirect('masters/keluarga/detailKeluarga/' . $nokk);
        }
    }

    public function getPenduduk()
    {
        $keyword = $this->input->post('cari');
        $query = "SELECT * FROM penduduk WHERE lower(namalengkap) LIKE '%$keyword%' OR lower(nik) LIKE '%$keyword%'";
        $penduduk = $this->db->query($query);

        if ($penduduk->num_rows() < 1) {
            echo json_encode(array());
        } else {
            $data = array();
            foreach ($penduduk->result_array() as $val) {
                $data[] = array('id' => $val['nik'], 'text' => $val['nik'] . ' - ' . $val['namalengkap']);
            }
            echo json_encode($data);
        }
    }

    public function deleteList($id, $key)
    {
        $ok = $this->db->delete('list_keluarga', ['idlist' => $id]);
        $ok ? showMessage('Berhasil menghapus data!', 'success') : showMessage('Gagal menghapus data!', 'danger');
        redirect('masters/keluarga/detailKeluarga/' . $key);
    }
}
