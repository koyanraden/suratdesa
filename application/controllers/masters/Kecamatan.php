<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kecamatan extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_kabupaten', 'kabupaten');
        $this->load->model('M_provinsi', 'provinsi');


        //set default
        $this->title = 'Data Kecamatan';
        $this->menu = 'kecamatan';
        $this->parent = 'masters';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {

        $a_kabupaten = $this->kabupaten->getListCombo();
        $a_provinsi = $this->provinsi->getListCombo();

        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'kecamatan', 'label' => 'Kecamatan'];
        $a_kolom[] = ['kolom' => 'idkabupaten', 'label' => 'Kabupaten', 'type' => 'S', 'option' => $a_kabupaten];
        $a_kolom[] = ['kolom' => 'idprovinsi', 'label' => 'Provinsi', 'type' => 'S', 'option' => $a_provinsi];

        $this->a_kolom = $a_kolom;
    }
}
