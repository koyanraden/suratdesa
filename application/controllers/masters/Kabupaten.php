<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kabupaten extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_provinsi', 'provinsi');


        //set default
        $this->title = 'Data Kabupaten';
        $this->menu = 'kabupaten';
        $this->parent = 'masters';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {
        $a_provinsi = $this->provinsi->getListCombo();

        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'kabupaten', 'label' => 'Kabupaten'];
        $a_kolom[] = ['kolom' => 'idprovinsi', 'label' => 'Provinsi', 'type' => 'S', 'option' => $a_provinsi];

        $this->a_kolom = $a_kolom;
    }
}
