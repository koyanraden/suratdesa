<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Desa extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_kecamatan', 'kecamatan');
        $this->load->model('M_kabupaten', 'kabupaten');
        $this->load->model('M_provinsi', 'provinsi');

        //set default
        $this->title = 'Data Desa';
        $this->menu = 'desa';
        $this->parent = 'masters';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {
        $a_kabupaten = $this->kabupaten->getListCombo();
        $a_kecamatan = $this->kecamatan->getListCombo();
        $a_provinsi = $this->provinsi->getListCombo();

        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'desa', 'label' => 'Nama Desa'];
        $a_kolom[] = ['kolom' => 'idkecamatan', 'label' => 'Kecamatan', 'type' => 'S', 'option' => $a_kecamatan];
        $a_kolom[] = ['kolom' => 'idkabupaten', 'label' => 'Kabupaten', 'type' => 'S', 'option' => $a_kabupaten];
        $a_kolom[] = ['kolom' => 'idprovinsi', 'label' => 'Provinsi', 'type' => 'S', 'option' => $a_provinsi];

        $this->a_kolom = $a_kolom;
    }
}
