<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penduduk extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        //set default
        $this->title = 'Data Penduduk';
        $this->menu = 'penduduk';
        $this->parent = 'masters';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {
        $a_kelamin = [
            '1' => 'Laki-Laki',
            '0' => 'Perempuan'
        ];

        $a_kewarganegaraan = [
            'WNI' => 'WNI',
            'WNA' => 'WNA'
        ];

        $a_kawin = [
            '1' => 'Sudah',
            '0' => 'Belum'
        ];

        $a_agama = [
            'Islam' => 'Islam',
            'Kristen' => 'Kristen',
            'Katholik' => 'Katholik',
            'Budha' => 'Budha',
            'Hindu' => 'Hindu',
            'Konghucu' => 'Konghucu'
        ];

        $a_pendidikan = [
            'Tidak Sekolah' => 'Tidak Sekolah',
            'SD' => 'SD',
            'SMP' => 'SMP',
            'SMA' => 'SMA',
            'D1-D3' => 'D1-D3',
            'Sarjana/D4' => 'Sarjana/D4',
            'Magister' => 'Magister',
            'Doktor' => 'Doktor'
        ];

        $a_pekerjaan = [
            'PNS' => 'PNS',
            'Wiraswasta' => 'Wiraswasta',
            'Pegawai' => 'Pegawai',
            'Pelajar/Mahasiswa' => 'Pelajar/Mahasiswa'
        ];

        $a_kolom = [];
        $a_kolom[] = ['kolom' => 'nik', 'label' => 'NIK', 'type' => 'N'];
        $a_kolom[] = ['kolom' => 'namalengkap', 'label' => 'Nama'];
        $a_kolom[] = ['kolom' => 'jeniskelamin', 'label' => 'Jenis Kelamin', 'type' => 'S', 'option' => $a_kelamin];
        $a_kolom[] = ['kolom' => 'tempatlahir', 'label' => 'Tempat Lahir'];
        $a_kolom[] = ['kolom' => 'tgl_lahir', 'label' => 'Tgl Lahir', 'type' => 'D'];
        $a_kolom[] = ['kolom' => 'is_kawin', 'label' => 'Status Kawin', 'type' => 'S', 'option' => $a_kawin];
        $a_kolom[] = ['kolom' => 'kewarganegaraan', 'label' => 'Kewarganegaraan', 'type' => 'S', 'option' => $a_kewarganegaraan];
        $a_kolom[] = ['kolom' => 'agama', 'label' => 'Agama', 'type' => 'S', 'option' => $a_agama];
        $a_kolom[] = ['kolom' => 'pendidikan', 'label' => 'Pendidikan', 'type' => 'S', 'option' => $a_pendidikan];
        $a_kolom[] = ['kolom' => 'jenispekerjaan', 'label' => 'Pekerjaan', 'type' => 'S', 'option' => $a_pekerjaan];
        $a_kolom[] = ['kolom' => 'alamat', 'label' => 'Alamat', 'type' => 'A', 'is_tampil' => false];

        $this->a_kolom = $a_kolom;
    }
}
