<?php

class Kontak extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_pesan', 'pesan');
    }

    public function index()
    {
        $data['title'] = 'Kontak kami';
        $data['nav'] = 'kontak';
        $data['users'] = $this->user;
        $this->front->load('client/contact/index', $data);

        if ($_POST) {
            $nama = $this->input->post('nama');
            $email = $this->input->post('email');
            $pesan = $this->input->post('pesan');

            $a_data = [
                'nama' => $nama,
                'email' => $email,
                'pesan' => $pesan
            ];

            $ok = $this->pesan->insert($a_data);
            $ok ? showMessage('Berhasil mengirim pesan', 'success') : showMessage('Gagal mengirim pesan, coba lagi!', 'danger');
            redirect('kontak');
        }
    }
}
