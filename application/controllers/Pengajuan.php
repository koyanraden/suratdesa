<?php

class Pengajuan extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function detail($id = null)
    {
        $data['title'] = 'Pengajuan Surat';
        $data['detail'] = $this->db->get_where('jenissurat', ['idjenissurat' => $id])->row_array();
        $nokk = $this->db->get_where('list_keluarga', ['nik' => $this->session->userdata('nik')])->row_array()['nokk'];
        $query = "SELECT * FROM list_keluarga JOIN penduduk USING(nik) WHERE nokk='$nokk'";
        $keluarga = $this->db->query($query)->result_array();
        $a_keluarga = array();
        foreach ($keluarga as $k) {
            if ($k['nik'] != $this->session->userdata('nik')) {
                $a_keluarga[] = $k;
            }
        }
        $data['nav'] = 'beranda';
        $data['keluarga'] = $a_keluarga;
        $data['jenissurat'] = $id;
        $this->front->load('client/pengajuan/detail', $data);
    }

    public function data()
    {
        if ($this->session->userdata('nik')) {
            if ($_POST || $_FILES) {
                $bukti = $_FILES['bukti'];
                $anggotakel = $this->input->post('anggotakel');
                $jenisusaha = $this->input->post('jenisusaha');
                $alasan = $this->input->post('alasan');
                $jenissurat = $this->input->post('jenissurat');
                $pindah_desa = $this->input->post('pindah_desa');
                $pindah_kecamatan = $this->input->post('pindah_kecamatan');
                $pindah_kabupaten = $this->input->post('pindah_kabupaten');
                $code = 'BT' . time();

                $cekPengajuan = $this->db->get_where('cetaksurat', ['nik' => $this->session->userdata('nik'), 'idjenissurat' => $jenissurat, 'status' => 0]);

                if ($cekPengajuan->num_rows() < 1) {
                    if ($bukti) {
                        $config['upload_path'] = './assets/bukti/';
                        $config['max_size'] = '2048';
                        $config['allowed_types'] = 'pdf';

                        $this->load->library('upload', $config);

                        if ($this->upload->do_upload('bukti')) {
                            $bukti = $this->upload->data('file_name');
                        } else {
                            showMessage($this->upload->display_errors(), 'danger');
                        }
                    }

                    $a_bukti = [
                        'idbukti' => $code,
                        'jenisusaha' => $jenisusaha,
                        'penduduk_meninggal' => $anggotakel,
                        'pindah_desa' => $pindah_desa,
                        'pindah_kecamatan' => $pindah_kecamatan,
                        'pindah_kabupaten' => $pindah_kabupaten,
                        'bukti' => $bukti,
                        'alasan' => $alasan
                    ];

                    $a_cetak = [
                        'idcetak' => 'CT' . time(),
                        'nik' => $this->session->userdata('nik'),
                        'idbukti' => $code,
                        'idjenissurat' => $jenissurat,
                        'status' => 0,
                    ];

                    $ok = $this->db->insert('bukti', $a_bukti);
                    $ok2 = $this->db->insert('cetaksurat', $a_cetak);

                    $ok && $ok2 ? showMessage('Berhasil melakukan pengajuan! pengajuan sedang kami proses, surat bisa diambil jika status pada menu pengajuanku telah menjadi hijau.', 'success') : showMessage('Pengajuan gagal, coba lagi!', 'danger');
                    redirect('pengajuan/detail/' . $jenissurat);
                } else {
                    showMessage('Anda sedang mengajukan surat yang sama!', 'danger');
                    redirect('pengajuan/detail/' . $jenissurat);
                }
            }
        } else {
            showMessage('Harap login terlebih dahulu!', 'danger');
            redirect('auth');
        }
    }

    public function pengajuanku()
    {
        $data['title'] = 'Pengajuanku';
        $data['nav'] = 'beranda';
        $nik = $this->session->userdata('nik');
        $query = "SELECT * FROM cetaksurat JOIN penduduk USING(nik) JOIN jenissurat USING(idjenissurat) WHERE nik='$nik'";
        $data['pengajuanku'] = $this->db->query($query)->result_array();
        $this->front->load('client/pengajuan/pengajuanku', $data);
    }

    public function cancel($id)
    {
        $ok = $this->db->delete('cetaksurat', ['idbukti' => $id]);
        $ok2 = $this->db->delete('bukti', ['idbukti' => $id]);

        $ok && $ok2 ? showMessage('Berhasil membatalkan pengajuan!', 'success') : showMessage('Gagal membatalkan pengajuan!', 'danger');
        redirect('pengajuan/pengajuanku');
    }
}
