<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MY_Controller
{
    public $user;

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_users', 'users');
        $this->user = $this->users->getBy(['username' => $this->session->userdata('username')])->row_array();
    }

    public function index()
    {
        $data['title'] = 'Dashboard';
        $data['user'] = $this->user;
        $a_info = array();
        $a_info[] = ['nama_info' => 'Jumlah Penduduk', 'value' => 1, 'icon' => 'users', 'color' => 'primary'];
        $a_info[] = ['nama_info' => 'Jumlah Keluarga', 'value' => 3, 'icon' => 'user-friends', 'color' => 'success'];
        $a_info[] = ['nama_info' => 'Jumlah Pengajuan', 'value' => 2, 'icon' => 'exchange-alt', 'color' => 'warning'];
        $data['info'] = $a_info;
        $this->template->load('template', 'home/index', $data);
    }
}
