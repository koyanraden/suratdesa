<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_pesan extends MY_Model
{
    protected $table = 'pesan';
    protected $schema = '';
    public $key = 'idpesan';
    public $value = 'pesan';

    function __construct()
    {
        parent::__construct();
    }
}
