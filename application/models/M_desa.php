<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_desa extends MY_Model
{
    protected $table = 'desa';
    protected $schema = '';
    public $key = 'iddesa';
    public $value = 'desa';

    function __construct()
    {
        parent::__construct();
    }
}
