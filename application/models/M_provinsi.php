<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_provinsi extends MY_Model
{
    protected $table = 'provinsi';
    protected $schema = '';
    public $key = 'idprovinsi';
    public $value = 'provinsi';

    function __construct()
    {
        parent::__construct();
    }
}
