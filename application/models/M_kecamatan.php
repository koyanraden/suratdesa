<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_kecamatan extends MY_Model
{
    protected $table = 'kecamatan';
    protected $schema = '';
    public $key = 'idkecamatan';
    public $value = 'kecamatan';

    function __construct()
    {
        parent::__construct();
    }
}
