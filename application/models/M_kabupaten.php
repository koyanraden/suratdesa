<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_kabupaten extends MY_Model
{
    protected $table = 'kabupaten';
    protected $schema = '';
    public $key = 'idkabupaten';
    public $value = 'kabupaten';

    function __construct()
    {
        parent::__construct();
    }
}
