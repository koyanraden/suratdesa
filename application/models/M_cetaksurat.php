<?php

class M_cetaksurat extends MY_Model
{
    protected $table = 'cetaksurat';
    protected $schema = '';
    public $key = 'idcetak';
    public $value = 'nik';

    function __construct()
    {
        parent::__construct();
    }

    public function getReffById($id)
    {
        $query = "SELECT * FROM cetaksurat cs JOIN jenissurat js ON cs.idjenissurat=js.idjenissurat JOIN penduduk p ON cs.nik=p.nik JOIN bukti b ON cs.idbukti=b.idbukti WHERE cs.idcetak='$id'";
        return $this->db->query($query);
    }
}
