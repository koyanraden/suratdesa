<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_rtrw extends MY_Model
{
    protected $table = 'rtrw';
    protected $schema = '';
    public $key = 'idrtrw';
    public $value = 'rtrw';

    function __construct()
    {
        parent::__construct();
    }
}
