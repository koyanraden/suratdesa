<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_penduduk extends MY_Model
{
    protected $table = 'penduduk';
    protected $schema = '';
    public $key = 'nik';
    public $value = 'namalengkap';

    function __construct()
    {
        parent::__construct();
    }
}
