<?php

class M_kartukeluarga extends MY_Model
{
    protected $table = 'kartukeluarga';
    protected $schema = '';
    protected $key = 'nokk';
    protected $value = 'alamat';

    function __construct()
    {
        parent::__construct();
    }

    public function getReff()
    {
        $query = "SELECT * FROM kartukeluarga kk JOIN rtrw r ON kk.idrtrw=r.idrtrw JOIN desa d ON kk.iddesa=d.iddesa JOIN kecamatan kc ON d.idkecamatan=kc.idkecamatan JOIN kabupaten kb ON d.idkabupaten=kb.idkabupaten JOIN provinsi p ON d.idprovinsi=p.idprovinsi";
        return $this->db->query($query);
    }
}
