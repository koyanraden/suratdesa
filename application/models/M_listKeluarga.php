<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_listKeluarga extends MY_Model
{
    protected $table = 'list_keluarga';
    protected $schema = '';
    public $key = 'idlist';
    public $value = 'nokk';

    function __construct()
    {
        parent::__construct();
    }

    public function getReff($id, $pos = null)
    {
        $cond = empty($pos) ? '' : " AND p.status_hub='$pos'";
        $query = "SELECT * FROM $this->table lk JOIN penduduk p ON lk.nik=p.nik WHERE lk.nokk='$id'" . $cond;
        return $this->db->query($query);
    }
}
