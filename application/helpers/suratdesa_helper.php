<?php

function settingSIM()
{
  $a_data = array();
  $ci = get_instance();
  $data = $ci->db->get('pengaturan')->result_array();

  foreach ($data as $val) {
    $a_data[$val['idpengaturan']] = $val['isi'];
  }

  return $a_data;
}

function showMessage($message, $type)
{
  $ci = get_instance();
  $message = $ci->session->set_flashdata('message', '<div class="alert alert-' . $type . ' alert-dismissible fade show" role="alert">' . $message . '               
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>');
}

function getMenu()
{
  return [
    'home' => [
      'key' => 'home',
      'name' => 'Beranda',
      'link' => 'home',
      'icon' => 'home'
    ],
    'master' => [
      'key' => 'master',
      'name' => 'Master',
      'link' => '',
      'icon' => 'tasks',
      'child' => [
        'users' => [
          'key' => 'users',
          'name' => 'Data User',
          'link' => 'masters/users'
        ],
        'penduduk' => [
          'key' => 'penduduk',
          'name' => 'Data Penduduk',
          'link' => 'masters/penduduk'
        ],
        'kartukeluarga' => [
          'key' => 'kartukeluarga',
          'name' => 'Data Keluarga',
          'link' => 'masters/keluarga'
        ],
        'provinsi' => [
          'key' => 'provinsi',
          'name' => 'Data Provinsi',
          'link' => 'masters/provinsi'
        ],
        'kabupaten' => [
          'key' => 'kabupaten',
          'name' => 'Data Kabupaten/Kota',
          'link' => 'masters/kabupaten'
        ],
        'kecamatan' => [
          'key' => 'kecamatan',
          'name' => 'Data Kecamatan',
          'link' => 'masters/kecamatan'
        ],
        'desa' => [
          'key' => 'desa',
          'name' => 'Data Desa',
          'link' => 'masters/desa'
        ],
        'rtrw' => [
          'key' => 'rtrw',
          'name' => 'Data RT/RW',
          'link' => 'masters/rtrw'
        ]
      ]
    ],
    'Surat' => [
      'key' => 'surat',
      'name' => 'Surat',
      'link' => '',
      'icon' => 'envelope',
      'child' => [
        'jenissurat' => [
          'key' => 'jenissurat',
          'name' => 'Jenis Surat',
          'link' => 'surat/jenissurat'
        ]
      ]
    ],
    'pengajuan' => [
      'key' => 'pengajuan',
      'name' => 'Pengajuan',
      'link' => '',
      'icon' => 'exchange-alt',
      'child' => [
        'pengajuan' => [
          'key' => 'pengajuan',
          'name' => 'Pengajuan Surat',
          'link' => 'surat/pengajuan'
        ]
      ]
    ],
    'pesan' => [
      'key' => 'pesan',
      'name' => 'Pesan Masuk',
      'icon' => 'envelope',
      'link' => 'pesan'
    ],
    'pengaturan' => [
      'key' => 'pengaturan',
      'name' => 'Pengaturan Aplikasi',
      'icon' => 'cogs',
      'link' => 'pengaturan'
    ],
    'logout' => [
      'key' => 'logout',
      'name' => 'Logout',
      'icon' => 'sign-out-alt',
      'link' => 'auth/logout'
    ]
  ];
}

function setPagination($limit = 10, $rows = null, $url, $model = null, $method = null)
{
  //instance ci
  $ci = get_instance();
  //load library
  $ci->load->library('pagination');

  $config['base_url'] = site_url($url);
  $config['total_rows'] = $rows;
  $config['per_page'] = $limit;
  $config['use_page_numbers'] = TRUE;
  $config['page_query_string'] = TRUE;

  //styling
  $config['full_tag_open'] = '<nav aria-label="Page navigation example"><ul class="pagination justify-content-center">';
  $config['full_tag_close'] = '</ul></nav>';

  $config['first_link'] = 'First';
  $config['first_tag_open'] = '<li class="page-item">';
  $config['first_tag_close'] = '</li>';

  $config['last_link'] = 'Last';
  $config['last_tag_open'] = '<li class="page-item">';
  $config['last_tag_close'] = '</li>';

  $config['next_link'] = '&raquo';
  $config['next_tag_open'] = '<li class="page-item">';
  $config['next_tag_close'] = '</li>';

  $config['prev_link'] = '&laquo';
  $config['prev_tag_open'] = '<li class="page-item">';
  $config['prev_tag_close'] = '</li>';

  $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
  $config['cur_tag_close'] = '</a></li>';

  $config['num_tag_open'] = '<li class="page-item">';
  $config['num_tag_close'] = '</li>';

  $config['attributes'] = array('class' => 'page-link');


  //init
  $ci->pagination->initialize($config);
}
