    <!-- ##### Hero Area Start ##### -->
    <section class="hero-area bg-img bg-overlay-2by5" style="background-image: url(<?= base_url('assets/img/home.jpg') ?>);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <!-- Hero Content -->
                    <div class="hero-content text-center">
                        <h2 style="text-shadow: 2px 2px 4px #000000;">Sistem Informasi Surat Desa</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Hero Area End ##### -->

    <!-- ##### Cool Facts Area Start ##### -->
    <section class="cool-facts-area section-padding-100-0">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <!-- Single Cool Facts Area -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-cool-facts-area text-center mb-100 wow fadeInUp" data-wow-delay="250ms">
                        <div class="icon">
                            <img src="<?= base_url('assets/img/core-img/earth.png') ?>" alt="">
                        </div>
                        <h2><span class="counter"><?= $a_data['penduduk'] ?></span></h2>
                        <h5>Penduduk</h5>
                    </div>
                </div>

                <!-- Single Cool Facts Area -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-cool-facts-area text-center mb-100 wow fadeInUp" data-wow-delay="750ms">
                        <div class="icon">
                            <img src="<?= base_url('assets/img/core-img/docs.png') ?>" alt="">
                        </div>
                        <h2><span class="counter"><?= $a_data['keluarga'] ?></span></h2>
                        <h5>Keluarga</h5>
                    </div>
                </div>

                <!-- Single Cool Facts Area -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-cool-facts-area text-center mb-100 wow fadeInUp" data-wow-delay="500ms">
                        <div class="icon">
                            <img src="<?= base_url('assets/img/core-img/star.png') ?>" alt="">
                        </div>
                        <h2><span class="counter"><?= $a_data['jenis_surat'] ?></span></h2>
                        <h5>Jenis Surat</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Cool Facts Area End ##### -->

    <!-- ##### Popular Courses Start ##### -->
    <section class="popular-courses-area section-padding-100-0" style="background-image: url(img/core-img/texture.png);">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading">
                        <h3>Daftar Jenis Surat</h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <!-- Single Popular Course -->
                <?php foreach ($a_jenis as $j) : ?>
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="single-popular-course mb-100 wow fadeInUp" data-wow-delay="250ms">
                            <img src="<?= base_url('assets/img/surat.jpg') ?>" alt="">
                            <!-- Course Content -->
                            <div class="course-content">
                                <h4></h4>
                                <div class="meta d-flex align-items-center">
                                    <table>
                                        <tr>
                                            <td>
                                                <h6><?= $j['jenissurat'] ?></h6>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="<?= base_url('pengajuan/detail/' . $j['idjenissurat']) ?>" class="btn btn-primary mt-2 text-white">Ajukan Surat</a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
    <!-- ##### Popular Courses End ##### -->