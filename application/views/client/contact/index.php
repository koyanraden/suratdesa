<!-- ##### Breadcumb Area Start ##### -->
<div class="breadcumb-area">
    <!-- Breadcumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Beranda</a></li>
            <li class="breadcrumb-item"><a href="#">Kontak</a></li>
        </ol>
    </nav>
</div>
<!-- ##### Breadcumb Area End ##### -->

<!-- ##### Contact Area Start ##### -->
<section class="contact-area">
    <div class="clever-catagory bg-img d-flex align-items-center justify-content-center p-3" style="background-image: url(<?= base_url('assets/img/partner/partner.jpg') ?>);">
        <h3>Kontak</h3>
    </div>
    <div class="container">
        <div class="row">
            <!-- Contact Info -->
            <div class="col-12 col-lg-6">
                <div class="contact--info mt-50 mb-100">
                    <h4>Kontak Info</h4>
                    <ul class="contact-list">
                        <li>
                            <h6><i class="fa fa-clock-o" aria-hidden="true"></i> Jam Kerja</h6>
                            <h6><?= settingSIM()['jam_kerja'] ?></h6>
                        </li>
                        <li>
                            <h6><i class="fa fa-phone" aria-hidden="true"></i> Nomor Telp</h6>
                            <h6><?= settingSIM()['call_center'] ?></h6>
                        </li>
                        <li>
                            <h6><i class="fa fa-envelope" aria-hidden="true"></i> Email</h6>
                            <h6><?= settingSIM()['email_center'] ?></h6>
                        </li>
                        <li>
                            <h6><i class="fa fa-map-pin" aria-hidden="true"></i> Alamat :</h6>
                        </li>
                        <li>
                            <h6><?= settingSIM()['alamat_kantor'] ?></h6>
                        </li>
                    </ul>
                </div>
            </div>

            <!-- Contact Form -->
            <div class="col-12 col-lg-6 mt-100 mb-50">
                <div class="contact-form">
                    <h4>Tinggalkan pesan</h4>
                    <?= $this->session->flashdata('message') ?>
                    <form action="<?= base_url('kontak') ?>" method="post">
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="nama" id="text" placeholder="Nama" required>
                                </div>
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <textarea name="pesan" class="form-control" id="pesan" cols="30" rows="10" placeholder="Pesan" required></textarea>
                                </div>
                            </div>
                            <div class="col-12">
                                <button type="submit" class="btn clever-btn w-100">Kirim <i class="fa fa-send"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>