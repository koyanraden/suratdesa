<!-- ##### Breadcumb Area Start ##### -->
<div class="breadcumb-area">
    <!-- Breadcumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Beranda</a></li>
            <li class="breadcrumb-item"><a href="#">Detail</a></li>
        </ol>
    </nav>
</div>
<!-- ##### Breadcumb Area End ##### -->

<!-- ##### Single Course Intro Start ##### -->
<div class="single-course-intro d-flex align-items-center justify-content-center" style="background-image: url(<?= base_url('assets/img/wall.jpg') ?>);">
    <!-- Content -->
    <div class="single-course-intro-content text-center">
        <!-- Ratings -->
        <h4 class="text-center"><?= $detail['jenissurat'] ?></h4>
    </div>
</div>
<!-- ##### Single Course Intro End ##### -->

<!-- ##### Courses Content Start ##### -->
<div class="single-course-content section-padding-100">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-12">
                <div class="course--content">

                    <div class="clever-tabs-content">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="tab--1" data-toggle="tab" href="#tab1" role="tab" aria-controls="tab1" aria-selected="false">Persyaratan</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="tab--2" data-toggle="tab" href="#tab2" role="tab" aria-controls="tab2" aria-selected="true">Form Pengajuan</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <!-- Tab Text -->
                            <div class="tab-pane fade show active" id="tab1" role="tabpanel" aria-labelledby="tab--1">
                                <div class="clever-description">

                                    <!-- About Course -->
                                    <div class="about-course mb-30">
                                        <?= $this->session->flashdata('message') ?>
                                        <h4>Persyaratan Pengajuan</h4>
                                        <?= $detail['persyaratan'] ?>
                                    </div>

                                </div>
                            </div>
                            <!-- Tab Text -->
                            <div class="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="tab--2">
                                <div class="clever-curriculum">

                                    <!-- About Curriculum -->
                                    <div class="about-curriculum mb-30">
                                        <h4>Form Pengajuan</h4>
                                        <div class="col-md-6">
                                            <?= $this->session->flashdata('message') ?>
                                            <form action="<?= base_url('pengajuan/data') ?>" method="POST" enctype="multipart/form-data">
                                                <div class="form-group">
                                                    <label for="bukti">Bukti</label>
                                                    <input type="file" class="form-control" name="bukti" id="bukti" required>
                                                    <small class="text-warning">Bukti harus berbentuk format pdf</small>
                                                </div>
                                                <?php if ($detail['idjenissurat'] == 1 && !empty($this->session->userdata('nik'))) : ?>
                                                    <div class="form-group">
                                                        <label for="anggotakel">Anggota keluarga (yang meninggal)</label>
                                                        <select name="anggotakel" id="anggotakel" class="form-control" required>
                                                            <option value="">-- Pilih anggota keluarga --</option>
                                                            <?php foreach ($keluarga as $k) : ?>
                                                                <option value="<?= $k['nik'] ?>"><?= $k['nik'] . ' - ' . $k['namalengkap'] ?></option>
                                                            <?php endforeach; ?>
                                                            <option value=""></option>
                                                        </select>
                                                    </div>
                                                <?php elseif ($detail['idjenissurat'] == 2) : ?>
                                                    <div class="form-group">
                                                        <label for="jenisusaha">Jenis Usaha</label>
                                                        <input type="text" class="form-control" id="jenisusaha" name="jenisusaha" placeholder="Masukkan jenis usaha" required>
                                                    </div>
                                                <?php elseif ($detail['idjenissurat'] == 4) : ?>
                                                    <div class="form-group">
                                                        <label for="pindah_desa">Desa tujuan</label>
                                                        <input type="text" class="form-control" id="pindah_desa" name="pindah_desa" placeholder="Masukkan desa tujuan" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="pindah_kecamatan">Kecamatan tujuan</label>
                                                        <input type="text" class="form-control" id="pindah_kecamatan" placeholder="Masukkan kecamatan tujuan" name="pindah_kecamatan" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="pindah_kabupaten">Kabupaten tujuan</label>
                                                        <input type="text" class="form-control" id="pindah_kabupaten" placeholder="Masukkan kabupaten tujuan" name="pindah_kabupaten" required>
                                                    </div>
                                                <?php endif; ?>
                                                <div class="form-group">
                                                    <label for="alasan">Alasan</label>
                                                    <textarea name="alasan" id="alasan" rows="5" class="form-control" placeholder="Masukkan alasan kegunaan surat" required></textarea>
                                                </div>
                                                <input type="hidden" name="jenissurat" id="jenissurat" value="<?= $jenissurat ?>">
                                                <div class="form-group">
                                                    <button class="btn btn-primary">Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Tab Text -->
                            <div class="tab-pane fade" id="tab3" role="tabpanel" aria-labelledby="tab--3">
                                <div class="clever-curriculum">

                                    <!-- About Curriculum -->
                                    <div class="about-curriculum mb-30">
                                        <h4>Jadwal</h4>
                                        <p align="justify">
                                            Sebelum anda mengikuti kursus <?= $detail['namapaketkursus'] ?> disarankan untuk melihat jadwal yang telah kami sediakan terlebih dahulu.
                                        </p>
                                        <a href="<?= base_url('assets/doc/datapaket/' . $detail['jadwal']) ?>" class="btn btn-primary" target="_blank">Download Jadwal <i class="fa fa-download ml-1"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>