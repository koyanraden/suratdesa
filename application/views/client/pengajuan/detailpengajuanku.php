    <!-- ##### Hero Area Start ##### -->
    <section class="hero-area bg-img bg-overlay-2by5" style="background-image: url(<?= base_url('assets/img/home.jpg'); ?>);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <!-- Hero Content -->
                    <div class="hero-content text-center">
                        <h2 style="text-shadow: 2px 2px 4px #000000;">Detail Kursus</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="container mt-5 mb-5">
        <div class="row d-flex justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <h5 class="card-header"><?= $detail['namapaketkursus'] ?></h5>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <img src="<?= base_url('assets/doc/datapaket/' . $detail['foto']) ?>" class="img-thumbnail" alt="">
                                <?php if (!empty($detail['buktibayar']) && empty($detail['noregistrasi'])) : ?>
                                    <img src="<?= base_url('assets/img/buktibayar/' . $detail['buktibayar']) ?>" class="img-thumbnail" alt="">
                                <?php endif; ?>
                            </div>
                            <div class="col-md-8">
                                <?= $this->session->flashdata('message') ?>
                                <h5 class="card-title">No Reg : <?= empty($detail['noregistrasi']) ? '-' : $detail['noregistrasi'] ?></h5>
                                <table class="table">
                                    <tr>
                                        <td>Nama Peserta</td>
                                        <td>:</td>
                                        <td><?= $detail['namapeserta'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Paket Kursus</td>
                                        <td>:</td>
                                        <td><?= $detail['namapaketkursus'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Jenis Kursus</td>
                                        <td>:</td>
                                        <td><?= $detail['jeniskursus'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Tempat Kursus</td>
                                        <td>:</td>
                                        <td><?= $detail['namakursus'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Waktu Daftar</td>
                                        <td>:</td>
                                        <td><?= $detail['waktu'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td>:</td>
                                        <td class="<?= $detail['status'] == 0 ? 'text-danger' : '' ?>"><?= $detail['status'] == 0 ? 'Belum bayar' : 'Sudah Bayar' ?></td>
                                    </tr>
                                </table>
                                <?php if (!empty($detail['noregistrasi'])) : ?>
                                    <a href="#" class="btn btn-primary">Cetak Bukti <i class="fa fa-print"></i></a>
                                <?php else : ?>
                                    <button type="button" data-type="btn-upload" class="btn btn-warning">Upload <i class="fa fa-upload ml-1"></i></button>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Upload Bukti Pembayaran</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?= base_url('kursus/uploadbukti') ?>" method="POST" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="buktibayar">Bukti Pembayaran</label>
                            <input type="file" name="buktibayar" id="buktibayar" class="form-control" required>
                        </div>
                        <input type="hidden" value="<?= $detail['idpembayaran'] ?>" name="idpembayaran">
                        <input type="hidden" value="<?= $detail['iddaftar'] ?>" name="iddaftar">
                        <input type="hidden" value="<?= $detail['buktibayar'] ?>" name="oldbukti">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Upload <i class="fa fa-upload ml-1"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script>
        $('[data-type=btn-upload]').click(function() {
            var modal = $('#exampleModalCenter');
            modal.modal();
        });
    </script>