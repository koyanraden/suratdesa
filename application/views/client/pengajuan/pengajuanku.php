    <!-- ##### Hero Area Start ##### -->
    <section class="hero-area bg-img bg-overlay-2by5" style="background-image: url(<?= base_url('assets/img/home.jpg'); ?>);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <!-- Hero Content -->
                    <div class="hero-content text-center">
                        <h2 style="text-shadow: 2px 2px 4px #000000;"><?= $title ?></h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container mt-5 mb-5">
        <div class="row d-flex justify-content-center">
            <div class="col-md-10">
                <div class="card shadow mb-4">
                    <div class="card-body">
                        <div class="row d-flex justify-content-center">
                            <div class="col-lg-12">
                                <?= $this->session->flashdata('message'); ?>
                                <form action="" method="post" id="table-list">
                                    <table class="table table-hover mt-5" id="data-table">
                                        <thead>
                                            <tr>
                                                <th scope="col">No</th>
                                                <th scope="col">NIK</th>
                                                <th scope="col">Nama Pengaju</th>
                                                <th scope="col">Jenis Surat</th>
                                                <th scope="col">Tanggal</th>
                                                <th scope="col">Status</th>
                                                <th scope="col">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; ?>
                                            <?php foreach ($pengajuanku as $p) : ?>
                                                <tr>
                                                    <th scope="row"><?= $i; ?></th>
                                                    <td><?= $p['nik']; ?></td>
                                                    <td><?= $p['namalengkap']; ?></td>
                                                    <td><?= $p['jenissurat']; ?></td>
                                                    <td><?= $p['request_at']; ?></td>
                                                    <td><span class="badge badge-<?= $p['status'] == 1 ? 'success' : 'danger'; ?>"><?= $p['status'] == 1 ? 'Sudah jadi' : ($p['status'] == 2 ? 'Ditolak' : 'Dalam Proses'); ?></span></td>
                                                    <td>
                                                        <a href="<?= base_url('pengajuan/cancel/' . $p['idbukti']) ?>" class="btn btn-sm btn-danger" data-toggle="tooltip" title="Batalkan"><i class="fa fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                                <?php $i++; ?>
                                            <?php endforeach; ?>
                                            <?php if (empty($pengajuanku)) : ?>
                                                <tr>
                                                    <td colspan="6" align="center">Anda belum mengajukan surat apapun.</td>
                                                </tr>
                                            <?php endif ?>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>