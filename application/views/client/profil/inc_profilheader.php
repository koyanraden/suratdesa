<div class="container mt-5">
    <div class="d-flex justify-content-center">
        <div class="row">
            <div class="col-md-12 mb-5 text-center">
                <ul class="nav nav-pills">
                    <li class="nav-item">
                        <a class="nav-link <?= $active == "profil" ? 'active' : ''  ?>" href="<?= base_url('profil') ?>">Profil</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?= $active == "edit" ? 'active' : ''  ?>" href="<?= base_url('profil/edit') ?>">Edit Profil</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?= $active == "password" ? 'active' : ''  ?>" href="<?= base_url('profil/password') ?>">Ganti Password</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>