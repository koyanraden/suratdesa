<div class="container mb-5">
    <div class="card mb-3 mx-auto" style="max-width: 540px;">
        <div class="row no-gutters">
            <div class="col-md-4">
                <img src="<?= base_url('assets/img/profil.png') ?>" class="card-img" alt="...">
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <table class="table">
                        <tr>
                            <td>Nama</td>
                            <td>:</td>
                            <td><?= $users['nama'] ?></td>
                        </tr>
                        <tr>
                            <td>Username</td>
                            <td>:</td>
                            <td><?= $users['username'] ?></td>
                        </tr>
                        <tr>
                            <td>Member Sejak</td>
                            <td>:</td>
                            <td><?= $users['date_created'] ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>