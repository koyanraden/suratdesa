<div class="container mt-2 mb-5">
    <div class="row d-flex justify-content-center">
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            <?= $this->session->flashdata('message') ?>
            <form action="<?= base_url('profil/edit') ?>" method="POST">
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan nama" value="<?= $users['nama'] ?>" required>
                </div>
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" class="form-control" name="username" id="username" placeholder="Masukkan username" value="<?= $users['username'] ?>" required>
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="Masukkan Email" value="<?= $users['email'] ?>" required>
                </div>
                <button type="submit" data-type="save" class="btn btn-primary btn-block"><i class="fa fa-save mr-2"></i> Simpan</button>
            </form>
        </div>
        <div class="col-md-2">
        </div>
    </div>
</div>