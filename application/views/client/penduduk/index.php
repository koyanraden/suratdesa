<!-- ##### Breadcumb Area Start ##### -->
<div class="breadcumb-area">
    <!-- Breadcumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Penduduk</a></li>
            <li class="breadcrumb-item"><a href="#">Detail</a></li>
        </ol>
    </nav>
</div>
<!-- ##### Breadcumb Area End ##### -->

<!-- ##### Single Course Intro Start ##### -->
<div class="single-course-intro d-flex align-items-center justify-content-center" style="background-image: url(<?= base_url('assets/img/wall.jpg') ?>);">
    <!-- Content -->
    <div class="single-course-intro-content text-center">
        <!-- Ratings -->
        <h4 class="text-center">Data Penduduk</h4>
    </div>
</div>
<!-- ##### Single Course Intro End ##### -->

<!-- ##### Courses Content Start ##### -->
<div class="single-course-content section-padding-100">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-12">
                <div class="course--content">

                    <div class="clever-tabs-content">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="tab--1" data-toggle="tab" href="#tab1" role="tab" aria-controls="tab1" aria-selected="false">Penduduk</a>
                            </li>
                        </ul>
                        <?= $this->session->flashdata('message') ?>
                        <div class="tab-content" id="myTabContent">
                            <!-- Tab Text -->
                            <div class="tab-pane fade show active" id="tab1" role="tabpanel" aria-labelledby="tab--1">
                                <div class="clever-description">

                                    <!-- About Course -->
                                    <div class="about-course mb-30">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h4>Pencarian Penduduk</h4>
                                                <form action="<?= base_url('penduduk'); ?>" method="post">
                                                    <div class="form-group">
                                                        <label for="nama">Nama Lengkap</label>
                                                        <input type="text" name="nama" id="nama" class="form-control" placeholder="Masukkan nama lengkap" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="tempatlahir">Tempat Lahir</label>
                                                        <input type="text" name="tempatlahir" id="tempatlahir" class="form-control" placeholder="Masukkan tempat lahir" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="tanggallahir">Tanggal Lahir</label>
                                                        <input type="date" name="tanggallahir" id="tanggallahir" class="form-control" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <button class="btn btn-primary" type="submit"><i class="fa fa-search mr-2"></i> Cari Data</button>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="col-md-6">
                                                <?php if ($pencarian != null) : ?>
                                                    <h6>Hasil Pencarian Nama : <?= $keyword ?></h6>
                                                    <table class="table table-hover">
                                                        <tr>
                                                            <td colspan="3"><i class="fa fa-user mr-2"></i> Informasi Penduduk</td>
                                                        </tr>
                                                        <tr>
                                                            <td>NIK</td>
                                                            <td>:</td>
                                                            <td><?= $pencarian['nik'] ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Nama Lengkap</td>
                                                            <td>:</td>
                                                            <td><?= $pencarian['namalengkap'] ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Jenis Kelamin</td>
                                                            <td>:</td>
                                                            <td><?= $pencarian['jenikelamin'] == 1 ? 'Laki-Laki' : 'Perempuan' ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tempat Lahir</td>
                                                            <td>:</td>
                                                            <td><?= $pencarian['tempatlahir'] ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tanggal Lahir</td>
                                                            <td>:</td>
                                                            <td><?= $pencarian['tgl_lahir'] ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Agama</td>
                                                            <td>:</td>
                                                            <td><?= $pencarian['agama'] ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Pendidikan Terakhir</td>
                                                            <td>:</td>
                                                            <td><?= $pencarian['pendidikan'] ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Pekerjaan</td>
                                                            <td>:</td>
                                                            <td><?= $pencarian['jenispekerjaan'] ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Status Kawin</td>
                                                            <td>:</td>
                                                            <td><?= $pencarian['is_kawin'] == 1 ? 'Sudah Kawin' : 'Belum Kawin' ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Kewarganegaraan</td>
                                                            <td>:</td>
                                                            <td><?= $pencarian['kewarganegaraan'] ?></td>
                                                        </tr>
                                                    </table>
                                                    <small class="mt-3">Tanggal Pencarian : <?= date('d-M-Y H:i:s') ?></small>
                                                <?php elseif (!empty($keyword) && $pencarian == null) : ?>
                                                    <h6>Hasil Pencarian Nama : <?= $keyword ?></h6>
                                                    <p class="text-danger"><i class="fa fa-search mr-2"></i> Penduduk tidak ditemukan</p>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>