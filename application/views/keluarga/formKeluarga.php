<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><?= $profile; ?></h1>
        <div class="card shadow mb-4">
            <div class="card-body">
                <form action="<?= base_url('masters/keluarga/formKeluarga') ?>" method="post" id="modal_post">
                    <div class="form-group">
                        <input type="text" class="form-control" id="nokk" name="nokk" placeholder="No Kartu Keluarga" value="<?= ($keluarga == null) ? '' : $keluarga['nokk'];  ?>">
                    </div>
                    <div class="form-group">
                        <textarea name="alamat" id="alamat" rows="3" class="form-control" placeholder="Alamat"><?= ($keluarga == null) ? '' : $keluarga['alamat'];  ?></textarea>
                    </div>
                    <div class="form-group">
                        <input type="number" class="form-control" id="kodepos" name="kodepos" placeholder="Kode Pos" value="<?= ($keluarga == null) ? '' : $keluarga['kodepos'];  ?>">
                    </div>
                    <div class="form-group">
                        <select name="desa" id="desa" class="form-control">
                            <option value="">-- Pilih Desa --</option>
                            <?php foreach ($desa as $d) : ?>
                                <option value="<?= $d['iddesa'] ?>"><?= $d['desa'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <select name="rtrw" id="rtrw" class="form-control">
                            <option value="">-- Pilih RT/RW --</option>
                            <?php foreach ($rtrw as $d) : ?>
                                <option value="<?= $d['idrtrw'] ?>"><?= $d['rtrw'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="button" data-type="back" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <input type="hidden" name="act" id="act" value="<?= $action; ?>">
                    <input type="hidden" name="key" id="key" value="<?= !empty($keluarga) ? $keluarga['nokk'] : '' ?>">
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $('[data-type=back]').click(function() {
        location.href = '<?= site_url('masters/keluarga') ?>';
    });

    let act = '<?= $keluarga != null ? 'edit' : 'simpan'  ?>';

    if (act == 'edit') {
        $('#desa').val('<?= $keluarga['iddesa'] ?>');
        $('#rtrw').val('<?= $keluarga['idrtrw'] ?>');
    }
</script>