<div id="content">
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><b><?= $title; ?></b></h1>

        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="col-md-4">
                    <table class="table">
                        <tr>
                            <td><b>Kepala Keluarga</b></td>
                            <td>:</td>
                            <td><?= empty($kepalakel) ? 'Belum ada' : $kepalakel['namalengkap'] ?></td>
                        </tr>
                        <tr>
                            <td><b>Jumlah Anggota</b></td>
                            <td>:</td>
                            <td><?= $jumlahKel; ?> Orang</td>
                        </tr>
                    </table>
                </div>
                <div class="mt-2">
                    <?= $this->session->flashdata('message') ?>
                </div>
                <table class="table">
                    <thead>
                        <th>No</th>
                        <th>Nama Lengkap</th>
                        <th>Jenis Kelamin</th>
                        <th>Tgl Lahir</th>
                        <th>Status Hub</th>
                        <th>Status Kawin</th>
                        <th>Aksi</th>
                    </thead>
                    <tbody>
                        <form action="<?= base_url('masters/keluarga/detailForm/') ?>" method="POST">
                            <tr>
                                <td colspan="2">
                                    <select name="penduduk" id="penduduk" class="form-control" required></select>
                                </td>
                                <td colspan="2">
                                    <select name="status_hub" id="status_hub" class="form-control">
                                        <option value="">-- Pilih status hub --</option>
                                        <option value="Kepala Keluarga">Kepala Keluarga</option>
                                        <option value="Istri">Istri</option>
                                        <option value="Anak">Anak</option>
                                    </select>
                                </td>
                                <td colspan="2">
                                    <select name="status_kawin" id="status_kawin" class="form-control">
                                        <option value="">-- Pilih status kawin --</option>
                                        <option value="0">Belum Menikah</option>
                                        <option value="1">Sudah Menikah</option>
                                    </select>
                                </td>
                                <td>
                                    <input type="hidden" name="nokk" id="nokk" value="<?= $nokartukeluarga ?>">
                                    <button type="submit" class="btn btn-sm btn-success">Tambah</button>
                                </td>
                            </tr>
                        </form>
                        <?php $i = 1; ?>
                        <?php foreach ($list_keluarga as $val) : ?>
                            <tr>
                                <td><?= $i++ ?></td>
                                <td><?= $val['namalengkap'] ?></td>
                                <td><?= $val['jeniskelamin'] == 1 ? 'Laki-Laki' : 'Perempuan' ?></td>
                                <td><?= $val['tgl_lahir'] ?></td>
                                <td><?= $val['status_hub'] ?></td>
                                <td><?= $val['is_kawin'] == 1 ? 'Sudah Menikah' : 'Belum Menikah' ?></td>
                                <td>
                                    <a href="<?= base_url('masters/keluarga/deleteList/' . $val['idlist'] . '/' . $val['nokk']) ?>" class="btn btn-sm btn-danger"><i class="fa fa-trash-alt"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    $('#penduduk').select2({
        minimumInputLength: 3,
        allowClear: true,
        placeholder: 'Masukkan nik penduduk',
        ajax: {
            dataType: 'json',
            type: 'POST',
            url: '<?= site_url('masters/keluarga/getPenduduk') ?>',
            delay: 250,
            data: function(params) {
                return {
                    cari: params.term
                }
            },
            processResults: function(data, page) {
                return {
                    results: data
                };
            },
        }
    });
</script>