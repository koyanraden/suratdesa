<div id="content">
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <button data-id="tambah" class="btn btn-primary mb-3" data-type="tambah">Tambah <?= $title ?></button>
                        <?= $this->session->flashdata('message'); ?>
                        <div class="table-responsive">
                            <table width="100%" class="table table-hover table-striped keluarga">
                                <thead>
                                    <tr>
                                        <th scope="col">No KK</th>
                                        <th scope="col">Alamat</th>
                                        <th scope="col">RT/RW</th>
                                        <th scope="col">Desa</th>
                                        <th scope="col">Kecamatan</th>
                                        <th scope="col">Kab/Kota</th>
                                        <th scope="col">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    <?php foreach ($keluarga as $p) : ?>
                                        <tr>
                                            <td><?= $p['nokk']; ?></td>
                                            <td><?= $p['alamat']; ?></td>
                                            <td><?= $p['rtrw'] ?></td>
                                            <td><?= $p['desa'] ?></td>
                                            <td><?= $p['kecamatan'] ?></td>
                                            <td><?= $p['kabupaten'] ?></td>
                                            <td>
                                                <button data-type="detail" data-id="<?= $p['nokk'] ?>" class="btn btn-sm btn-info">Detail</button>
                                                <button data-type="edit" data-id="<?= $p['nokk'] ?>" class="btn btn-sm btn-primary">Edit</button>
                                                <button data-type="btndelete" data-id="<?= $p['nokk'] ?>" class="btn btn-sm btn-danger">Hapus</button>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of Main Content -->

            <!-- End of Main Content -->
            <div class="modal" tabindex="-1" role="dialog" id="modal-delete">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Hapus Keluarga</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Apakah anda ingin menghapus keluarga ini?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            <button type="button" data-type="delete" data-id="" class="btn btn-danger" data-dismiss="modal">Hapus</button>
                        </div>
                    </div>
                </div>
            </div>

            <script>
                $('[data-type=tambah]').click(function() {
                    var id = $(this).attr('data-id');
                    location.href = '<?= site_url('masters/keluarga/formKeluarga/') ?>' + id;
                });

                $('[data-type=edit]').click(function() {
                    var id = $(this).attr('data-id');
                    location.href = '<?= site_url('masters/keluarga/formKeluarga/') ?>' + id;
                });

                $('[data-type=detail]').click(function() {
                    var id = $(this).attr('data-id');
                    location.href = '<?= site_url('masters/keluarga/detailKeluarga/') ?>' + id;
                });

                $('[data-type=btndelete]').click(function() {
                    var id = $(this).attr('data-id');
                    var modal = $('#modal-delete');
                    modal.find('[data-type=delete]').attr('data-id', id);
                    modal.modal();
                });

                $('[data-type=delete]').click(function() {
                    var id = $(this).attr('data-id');
                    location.href = '<?= site_url('masters/keluarga/deleteKeluarga/') ?>' + id;
                });

                $('.keluarga').DataTable();
            </script>
            <!-- End of Main Content -->
        </div>
    </div>
</div>