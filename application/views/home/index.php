<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-6">
                <?= $this->session->flashdata('message'); ?>
            </div>
        </div>
        <div class="jumbotron">
            <h1 class="display-4">Selamat datang, <?= $user['nama']; ?></h1>
            <p class="lead">Sistem Informasi <?= settingSIM()['nama_aplikasi'] ?></p>
        </div>
        <!-- /.container-fluid -->
        <div class="row">
            <?php foreach ($info as $i) : ?>
                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-<?= $i['color'] ?> shadow h-100 py-2" id="siswa">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-<?= $i['color'] ?> text-uppercase mb-1"><?= $i['nama_info'] ?></div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $i['value'] ?></div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-<?= $i['icon'] ?> fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

    </div>
    <!-- End of Main Content -->
</div>