<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Surat Keterangan Pindah</title>
    <style>
        .table1 {
            font-family: sans-serif;
            color: #232323;
            border-collapse: collapse;
        }

        .table1,
        th,
        td {
            border: 1px solid #0000;
            padding: 8px 20px;
        }

        .table2 {
            font-family: sans-serif;
            color: #232323;
            border-collapse: collapse;
        }

        .table2,
        th,
        td {
            border: 0px;
            padding: 8px 20px;
        }
    </style>
</head>

<body>
    <?= settingSIM()['kop_surat'] ?>
    <p align="center" style="margin-top: -250px;"><b><?= strtoupper($a_data['jenissurat']) ?></b></p>
    <p align="center" style="margin-top: -240px;">Nomor : <?= $nosurat ?></p>
    <br>
    <p>Saya yang bertanda tangan di bawah, Kepala Desa <?= settingSIM()['nama_desa'] ?> menerangkan bahwa :</p>
    <table class="table1">
        <tr>
            <td><b>No</b></td>
            <td><b>Nama</b></td>
            <td><b>Jenis Kelamin</b></td>
            <td><b>TTL</b></td>
            <td><b>Status</b></td>
            <td><b>Alamat</b></td>
        </tr>
        <tbody>
            <?php $i = 1; ?>
            <?php foreach ($keluarga as $kel) : ?>
                <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $kel['namalengkap']; ?></td>
                    <td><?= $kel['jeniskelamin'] == 1 ? 'Laki-Laki' : 'Perempuan'; ?></td>
                    <td><?= $kel['tempatlahir'] . ', ' . $kel['tgl_lahir']; ?></td>
                    <td><?= $kel['is_kawin'] == 1 ? 'Sudah Menikah' : 'Belum Menikah'; ?></td>
                    <td><?= $kel['alamat']; ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <p align="justify" style="line-height: 2;">Dengan ini menerangkan bahwa nama tersebut diatas adalah Penduduk Desa <?= settingSIM()['nama_desa'] ?> Kecamatan <?= settingSIM()['nama_kecamatan'] ?> Kabupaten <?= settingSIM()['nama_kabupaten'] ?> yang saat ini sudah pindah domisili di Desa <?= $a_data['pindah_desa'] ?> Kecamatan <?= $a_data['pindah_kecamatan'] ?> Kabupaten <?= $a_data['pindah_kabupaten'] ?>. <br>
        Demikian surat keterangan ini dikeluarkan kepada yang bersangkutan untuk dipergunakan sebagaimana mestinya.
    </p>
    <table align="right" class="table2" style="margin-top: 50px;">
        <tr>
            <td><?= settingSIM()['nama_kabupaten'] . ', ' . date('d-M-Y') ?></td>
        </tr>
        <tr>
            <td>Kepala Desa <?= settingSIM()['nama_desa'] ?></td>
        </tr>
        <tr style="height: 120px">
            <td></td>
        </tr>
        <tr style="height: 120px">
            <td></td>
        </tr>
        <tr style="height: 120px">
            <td></td>
        </tr>
        <tr>
            <td><?= settingSIM()['kepala_desa'] ?></td>
        </tr>
    </table>

</body>

</html>