<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Surat Keterangan Kematian</title>
    <style>
        .table1 {
            font-family: sans-serif;
            color: #232323;
            border-collapse: collapse;
        }

        .table1,
        th,
        td {
            border: 0px;
            padding: 8px 20px;
        }
    </style>
</head>

<body>
    <?= settingSIM()['kop_surat'] ?>
    <p align="center" style="margin-top: -250px;"><b><?= strtoupper($a_data['jenissurat']) ?></b></p>
    <p align="center" style="margin-top: -240px;">Nomor : <?= $nosurat ?></p>
    <br>
    <p>Saya yang bertanda tangan di bawah, Kepala Desa <?= settingSIM()['nama_desa'] ?> menerangkan dengan sesungguhnya bahwa :</p>
    <table class="table1">
        <tr>
            <td>NIK</td>
            <td>:</td>
            <td><?= $penduduk_meninggal['nik'] ?></td>
        </tr>
        <tr>
            <td>Nama Lengkap</td>
            <td>:</td>
            <td><?= $penduduk_meninggal['namalengkap'] ?></td>
        </tr>
        <tr>
            <td>Jenis Kelamin</td>
            <td>:</td>
            <td><?= $penduduk_meninggal['jeniskelamin'] == 1 ? 'Laki-Laki' : 'Perempuan' ?></td>
        </tr>
        <tr>
            <td>TTL</td>
            <td>:</td>
            <td><?= $penduduk_meninggal['tempatlahir'] . ', ' . $penduduk_meninggal['tgl_lahir'] ?></td>
        </tr>
        <tr>
            <td>Agama</td>
            <td>:</td>
            <td><?= $penduduk_meninggal['agama'] ?></td>
        </tr>
        <tr>
            <td>Kewarganegaraan</td>
            <td>:</td>
            <td><?= $penduduk_meninggal['kewarganegaraan'] ?></td>
        </tr>
        <tr>
            <td>Pekerjaan</td>
            <td>:</td>
            <td><?= $penduduk_meninggal['jenispekerjaan'] ?></td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>:</td>
            <td><?= $penduduk_meninggal['alamat'] ?></td>
        </tr>
    </table>
    <p align="justify" style="line-height: 2;">Telah benar-benar meninggal dunia. <br>

        Demikian surat keterangan kematian ini dibuat dengan sebenarnya dan agar dapat dipergunakan dengan semestinya.</p>

    <table align="right" class="table1" style="margin-top: 60px;">
        <tr>
            <td><?= settingSIM()['nama_kabupaten'] . ', ' . date('d-M-Y') ?></td>
        </tr>
        <tr>
            <td>Kepala Desa <?= settingSIM()['nama_desa'] ?></td>
        </tr>
        <tr style="height: 120px">
            <td></td>
        </tr>
        <tr style="height: 120px">
            <td></td>
        </tr>
        <tr style="height: 120px">
            <td></td>
        </tr>
        <tr>
            <td><?= settingSIM()['kepala_desa'] ?></td>
        </tr>
    </table>

</body>

</html>