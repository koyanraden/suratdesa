<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Surat Keterangan Usaha</title>
    <style>
        .table1 {
            font-family: sans-serif;
            color: #232323;
            border-collapse: collapse;
        }

        .table1,
        th,
        td {
            border: 0px;
            padding: 8px 20px;
        }
    </style>
</head>

<body>
    <?= settingSIM()['kop_surat'] ?>
    <p align="center" style="margin-top: -250px;"><b><?= strtoupper($a_data['jenissurat']) ?></b></p>
    <p align="center" style="margin-top: -240px;">Nomor : <?= $nosurat ?></p>
    <br>
    <p>Saya yang bertanda tangan di bawah, Kepala Desa <?= settingSIM()['nama_desa'] ?> menerangkan bahwa :</p>
    <table class="table1">
        <tr>
            <td>NIK</td>
            <td>:</td>
            <td><?= $a_data['nik'] ?></td>
        </tr>
        <tr>
            <td>Nama Lengkap</td>
            <td>:</td>
            <td><?= $a_data['namalengkap'] ?></td>
        </tr>
        <tr>
            <td>Jenis Kelamin</td>
            <td>:</td>
            <td><?= $a_data['jeniskelamin'] == 1 ? 'Laki-Laki' : 'Perempuan' ?></td>
        </tr>
        <tr>
            <td>TTL</td>
            <td>:</td>
            <td><?= $a_data['tempatlahir'] . ', ' . $a_data['tgl_lahir'] ?></td>
        </tr>
        <tr>
            <td>Pekerjaan</td>
            <td>:</td>
            <td><?= $a_data['jenispekerjaan'] ?></td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>:</td>
            <td><?= $a_data['alamat'] ?></td>
        </tr>
    </table>
    <p align="justify" style="line-height: 2;">Nama yang tertera diatas adalah benar-benar penduduk kami yang berdomisili Desa <?= settingSIM()['nama_desa'] ?> Kecamatan <?= settingSIM()['nama_kecamatan'] ?> Kabupaten <?= settingSIM()['nama_kabupaten'] ?>.

        Berdasarkan pengamatan Kami bahwa nama tersebut diatas benar memiliki Usaha <?= $a_data['jenisusaha'] ?> di Desa <?= settingSIM()['nama_desa'] ?>.

        Demikian surat keterangan usaha ini dibuat dengan sebenarnya dan agar dapat dipergunakan dengan semestinya.</p>

    <table align="right" class="table1" style="margin-top: 120px;">
        <tr>
            <td><?= settingSIM()['nama_kabupaten'] . ', ' . date('d-M-Y') ?></td>
        </tr>
        <tr>
            <td>Kepala Desa <?= settingSIM()['nama_desa'] ?></td>
        </tr>
        <tr style="height: 120px">
            <td></td>
        </tr>
        <tr style="height: 120px">
            <td></td>
        </tr>
        <tr style="height: 120px">
            <td></td>
        </tr>
        <tr>
            <td><?= settingSIM()['kepala_desa'] ?></td>
        </tr>
    </table>

</body>

</html>