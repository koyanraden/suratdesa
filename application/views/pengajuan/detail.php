<div class="content">
    <div class="container-fluid">
        <h1 class="h3 mb-4 text-gray-800"><b><?= $title; ?></b></h1>
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <form action="<?= base_url('surat/pengajuan/detail'); ?>" method="post">
                            <div class="row mb-5">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <input type="text" name="nosurat" class="form-control" placeholder="Masukkan No Surat" aria-label="No Surat" aria-describedby="button-addon4" autocomplete="off" required>
                                        <input type="hidden" name="idcetak" value="<?= $a_data['idcetak'] ?>">
                                        <div class="input-group-append" id="button-addon4">
                                            <button class="btn btn-primary" type="submit"><i class="fa fa-print mr-2"></i>Cetak Surat</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <h6><i class="fa fa-user mr-2 mb-3"></i><b>Informasi Pengaju</b></h6>
                        <?= $this->session->flashdata('message') ?>
                        <table class="table mt-3">
                            <?php foreach ($a_kolom as $kolom) : ?>
                                <tr>
                                    <td><?= $kolom['kolom'] ?></td>
                                    <td>:</td>
                                    <td><?= $kolom['value'] ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <h6><i class="fa fa-envelope mr-2 mb-3"></i><b>Informasi Surat</b></h6>
                        <?= $this->session->flashdata('message') ?>
                        <table class="table mt-3 mb-3">
                            <?php foreach ($a_surat as $surat) : ?>
                                <tr>
                                    <td><?= $surat['kolom'] ?></td>
                                    <td>:</td>
                                    <?php if ($surat['type'] == 'F') : ?>
                                        <td><a href="<?= $surat['path'] . $surat['value'] ?>" target="_blank"><?= $surat['value'] ?></a></td>
                                    <?php else : ?>
                                        <?php if (isset($justify)) : ?>
                                            <td><?= $surat['value'] ?></td>
                                        <?php else : ?>
                                            <td>
                                                <p justify="<?= $surat['justify'] ?>"><?= $surat['value'] ?></p>
                                            </td>
                                        <?php endif; ?>
                                    <?php endif ?>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>