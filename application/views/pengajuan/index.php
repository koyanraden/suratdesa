<div id="content">
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><b><?= $title; ?></b></h1>

        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <?= $this->session->flashdata('message'); ?>
                        <div class="table-responsive">
                            <table width="100%" class="table table-hover table-striped pengaturan">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">ID Cetak</th>
                                        <th scope="col">Nama Pengaju</th>
                                        <th scope="col">Jenis Surat</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Tanggal</th>
                                        <th scope="col">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    <?php foreach ($pengajuan as $p) : ?>
                                        <tr>
                                            <td><?= $i++; ?></td>
                                            <td><?= $p['idcetak'] ?></td>
                                            <td><?= $p['namalengkap'] ?></td>
                                            <td><?= $p['jenissurat'] ?></td>
                                            <td><span class="badge badge-<?= $p['status'] == 1 ? 'success' : 'danger' ?>"><?= $p['status'] == 1 ? 'Sudah dicetak' : ($p['status'] == 2 ? 'Ditolak' : 'Belum dicetak') ?></span></td>
                                            <td><?= $p['request_at'] ?></td>
                                            <td>
                                                <a href="<?= base_url('surat/pengajuan/detail/' . $p['idcetak']) ?>" class="btn btn-sm btn-info">Detail</a>
                                                <a href="<?= base_url('surat/pengajuan/tolak/' . $p['idcetak']) ?>" class="btn btn-sm btn-danger">Tolak</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of Main Content -->

            <!-- End of Main Content -->
            <div class="modal" tabindex="-1" role="dialog" id="modal-delete">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Hapus Pengaturan</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Apakah anda ingin menghapus pengaturan ini?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            <button type="button" data-type="delete" data-id="" class="btn btn-danger" data-dismiss="modal">Hapus</button>
                        </div>
                    </div>
                </div>
            </div>

            <script>
                $('[data-type=tambah]').click(function() {
                    var id = $(this).attr('data-id');
                    location.href = '<?= site_url('pengaturan/formPengaturan/') ?>' + id;
                });
                $('[data-type=edit]').click(function() {
                    var id = $(this).attr('data-id');
                    location.href = '<?= site_url('pengaturan/formPengaturan/') ?>' + id;
                });

                $('[data-type=btndelete]').click(function() {
                    var id = $(this).attr('data-id');
                    var modal = $('#modal-delete');
                    modal.find('[data-type=delete]').attr('data-id', id);
                    modal.modal();
                });

                $('[data-type=delete]').click(function() {
                    var id = $(this).attr('data-id');
                    location.href = '<?= site_url('pengaturan/deletePengaturan/') ?>' + id;
                });

                $('.pengaturan').DataTable();
            </script>
            <!-- End of Main Content -->
        </div>
    </div>
</div>