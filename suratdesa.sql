-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 24 Apr 2020 pada 10.36
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `suratdesa`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `bukti`
--

CREATE TABLE `bukti` (
  `idbukti` varchar(64) NOT NULL,
  `jenisusaha` varchar(255) DEFAULT NULL,
  `penduduk_meninggal` varchar(128) DEFAULT NULL,
  `pindah_desa` varchar(255) DEFAULT NULL,
  `pindah_kecamatan` varchar(255) DEFAULT NULL,
  `pindah_kabupaten` varchar(255) DEFAULT NULL,
  `bukti` varchar(255) DEFAULT NULL,
  `alasan` text DEFAULT NULL,
  `insert_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `bukti`
--

INSERT INTO `bukti` (`idbukti`, `jenisusaha`, `penduduk_meninggal`, `pindah_desa`, `pindah_kecamatan`, `pindah_kabupaten`, `bukti`, `alasan`, `insert_at`) VALUES
('BT1587688174', NULL, NULL, 'Asri', 'Rungkut', 'Surabaya', 'contoh_bukti.pdf', 'Digunakan sebagai surat pengantar pengajuan npwp usaha', '2020-04-24 00:29:34'),
('BT1587717118', NULL, NULL, 'Duduk Sampeyan', 'Duduk', 'Gresik', 'file1.pdf', 'untuk pengurusan kk baru', '2020-04-24 08:31:58');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cetaksurat`
--

CREATE TABLE `cetaksurat` (
  `idcetak` varchar(64) NOT NULL,
  `nik` varchar(64) DEFAULT NULL,
  `idbukti` varchar(64) DEFAULT NULL,
  `nosurat` varchar(128) DEFAULT NULL,
  `idjenissurat` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `request_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `cetak_by` int(11) DEFAULT NULL,
  `cetak_at` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `cetaksurat`
--

INSERT INTO `cetaksurat` (`idcetak`, `nik`, `idbukti`, `nosurat`, `idjenissurat`, `status`, `request_at`, `cetak_by`, `cetak_at`) VALUES
('CT1587688174', '1000122', 'BT1587688174', 'SR0001', 4, 1, '2020-04-24 00:29:34', 1, '24-Apr-2020 10:19:25'),
('CT1587717118', '1000123', 'BT1587717118', NULL, 4, 0, '2020-04-24 08:31:58', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `desa`
--

CREATE TABLE `desa` (
  `iddesa` int(11) NOT NULL,
  `desa` varchar(255) DEFAULT NULL,
  `idkecamatan` int(11) DEFAULT NULL,
  `idkabupaten` int(11) DEFAULT NULL,
  `idprovinsi` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `desa`
--

INSERT INTO `desa` (`iddesa`, `desa`, `idkecamatan`, `idkabupaten`, `idprovinsi`) VALUES
(1, 'Medokan Asri', 1, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenissurat`
--

CREATE TABLE `jenissurat` (
  `idjenissurat` int(11) NOT NULL,
  `jenissurat` varchar(128) DEFAULT NULL,
  `persyaratan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `jenissurat`
--

INSERT INTO `jenissurat` (`idjenissurat`, `jenissurat`, `persyaratan`) VALUES
(1, 'Surat Keterangan Kematian', '<p>Persyaratan Pengajuan Surat Keterangan Kematian</p>\r\n<ul>\r\n<li>Surat keterangan dari RT/RW Setempat</li>\r\n</ul>\r\n<p>&nbsp;</p>'),
(2, 'Surat Keterangan Usaha', '<p>Persyaratan pengajuan Surat Keterangan Usaha</p>\r\n<ul>\r\n<li>Surat pengantar dari RT setempat</li>\r\n<li>Bukti Usaha</li>\r\n</ul>'),
(3, 'Surat Keterangan Domisili', '<p>Persyaratan pengajuan Surat Keterangan&nbsp;Domisili</p>\r\n<ul>\r\n<li>Surat pengantar dari RT setempat</li>\r\n<li>Bukti Domisili</li>\r\n</ul>'),
(4, 'Surat Keterangan Pindah', '<p>Persyaratan pengajuan Surat Keterangan&nbsp;Pindah</p>\r\n<ul>\r\n<li>Surat pengantar dari RT setempat</li>\r\n<li>Bukti Pendukung</li>\r\n</ul>'),
(5, 'Surat Keterangan Belum Menikah', '<p>Persyaratan pengajuan Surat Keterangan&nbsp;Belum Menikah</p>\r\n<ul>\r\n<li>Surat pengantar dari RT setempat</li>\r\n<li>Bukti&nbsp;Pendukung lainnya</li>\r\n</ul>');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kabupaten`
--

CREATE TABLE `kabupaten` (
  `idkabupaten` int(11) NOT NULL,
  `kabupaten` varchar(255) DEFAULT NULL,
  `idprovinsi` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kabupaten`
--

INSERT INTO `kabupaten` (`idkabupaten`, `kabupaten`, `idprovinsi`) VALUES
(1, 'Surabaya', 1),
(3, 'Gresik', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kartukeluarga`
--

CREATE TABLE `kartukeluarga` (
  `nokk` varchar(128) NOT NULL,
  `alamat` text DEFAULT NULL,
  `idrtrw` int(64) DEFAULT NULL,
  `kodepos` varchar(32) DEFAULT NULL,
  `iddesa` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kartukeluarga`
--

INSERT INTO `kartukeluarga` (`nokk`, `alamat`, `idrtrw`, `kodepos`, `iddesa`) VALUES
('351112', 'JL.Medokan Asri Surabaya', 1, '62118', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kecamatan`
--

CREATE TABLE `kecamatan` (
  `idkecamatan` int(11) NOT NULL,
  `kecamatan` varchar(255) DEFAULT NULL,
  `idkabupaten` int(11) DEFAULT NULL,
  `idprovinsi` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kecamatan`
--

INSERT INTO `kecamatan` (`idkecamatan`, `kecamatan`, `idkabupaten`, `idprovinsi`) VALUES
(1, 'Rungkut', 1, 1),
(2, 'Ngagel', 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `list_keluarga`
--

CREATE TABLE `list_keluarga` (
  `idlist` int(11) NOT NULL,
  `nokk` varchar(64) DEFAULT NULL,
  `nik` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `list_keluarga`
--

INSERT INTO `list_keluarga` (`idlist`, `nokk`, `nik`) VALUES
(8, '351112', '1000123'),
(9, '351112', '1000121'),
(10, '351112', '1000122');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penduduk`
--

CREATE TABLE `penduduk` (
  `nik` varchar(64) NOT NULL,
  `namalengkap` varchar(255) DEFAULT NULL,
  `jeniskelamin` int(11) DEFAULT NULL,
  `tempatlahir` varchar(255) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `agama` varchar(64) DEFAULT NULL,
  `pendidikan` varchar(64) DEFAULT NULL,
  `jenispekerjaan` varchar(64) DEFAULT NULL,
  `is_kawin` int(11) DEFAULT NULL,
  `status_hub` varchar(128) DEFAULT NULL,
  `kewarganegaraan` varchar(32) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `insert_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `penduduk`
--

INSERT INTO `penduduk` (`nik`, `namalengkap`, `jeniskelamin`, `tempatlahir`, `tgl_lahir`, `agama`, `pendidikan`, `jenispekerjaan`, `is_kawin`, `status_hub`, `kewarganegaraan`, `alamat`, `insert_at`) VALUES
('1000121', 'Iqbal', 1, 'Surabaya', '2020-04-20', 'Islam', 'Sarjana/D4', 'PNS', 0, 'Anak', 'WNI', 'JL.Medokan Asri Surabaya', '2020-04-20 02:41:53'),
('1000122', 'Hendra', 1, 'Surabaya', '2020-04-08', 'Islam', 'Sarjana/D4', 'Wiraswasta', 0, 'Anak', 'WNI', 'JL.Medokan Asri Surabaya', '2020-04-23 09:24:25'),
('1000123', 'Budi Sudarsono', 1, 'Surabaya', '2000-11-24', 'Islam', 'Sarjana/D4', 'Wiraswasta', 1, 'Kepala Keluarga', 'WNI', 'JL.Medokan Asri Surabaya', '2020-04-19 09:41:34');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengaturan`
--

CREATE TABLE `pengaturan` (
  `idpengaturan` varchar(128) NOT NULL,
  `namapengaturan` varchar(128) DEFAULT NULL,
  `isi` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pengaturan`
--

INSERT INTO `pengaturan` (`idpengaturan`, `namapengaturan`, `isi`) VALUES
('alamat_kantor', 'Alamat Kantor Desa', 'JL.Ngagel No.32, Surabaya'),
('call_center', 'Call Center', '083423'),
('email_center', 'Email Center', 'kantordesa@gmail.com'),
('jam_kerja', 'Jam Kerja', '09:00-14:00 WIB'),
('kepala_desa', 'Kepala desa', 'Rahman Asidiq, S.Sos'),
('kop_surat', 'Kop Surat', '<h3 class=\"ff0\" style=\"text-align: center;\"><strong><span class=\"a\">PEMERINTAH&nbsp;<span class=\"l\">KOTA SURABAYA&nbsp;</span></span><span class=\"a\">KECAMATAN&nbsp;NGAGEL</span></strong></h3>\r\n<h3 class=\"ff0\" style=\"text-align: center;\"><strong><span class=\"a\">KEPALA&nbsp;</span></strong><strong><span class=\"a\">DESA&nbsp;MEDOKAN</span></strong></h3>\r\n<p style=\"text-align: center;\"><span class=\"a\">Jln. Ngagel Medokan Asri No.33<span class=\"l6\">, Ngagel-Surabaya Kode Pos 41353</span></span><span class=\"a\">&nbsp;</span></p>\r\n<hr />\r\n<p style=\"text-align: center;\">&nbsp;</p>'),
('nama_aplikasi', 'Nama Aplikasi', 'Surat Desa'),
('nama_desa', 'Nama Desa', 'Medokan'),
('nama_kabupaten', 'Nama Kabupaten / Kota', 'Surabaya'),
('nama_kecamatan', 'Nama Kecamatan', 'Ngagel'),
('nama_provinsi', 'Nama Provinsi', 'Jawa Timur'),
('nip_kepala_desa', 'NIP Kepala Desa', '10009283'),
('pengajuan_diterima', 'Pernyataan Pengajuan', '<p>Selamat pengajuan surat anda telah memenuhi persyaratan dan silahkan bisa di ambil di kantor kepala desa</p>\r\n<p>Terimakasih</p>'),
('pengajuan_ditolak', 'Pernyataan Pengajuan', '<p>Maaf dokumen yang anda jadikan persyaratan tidak memenuhi persyaratan kami. Untuk lebih jelasnya anda dapat mengajukan surat tersebut secara langsung dengan datang ke kantor kepala desa.</p>\r\n<p>Terimakasih</p>');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pesan`
--

CREATE TABLE `pesan` (
  `idpesan` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `pesan` text DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pesan`
--

INSERT INTO `pesan` (`idpesan`, `nama`, `email`, `pesan`, `time`) VALUES
(1, 'Budi Sudarsono', 'budi@gmail.com', 'test', '2020-04-23 14:56:07');

-- --------------------------------------------------------

--
-- Struktur dari tabel `provinsi`
--

CREATE TABLE `provinsi` (
  `idprovinsi` int(11) NOT NULL,
  `provinsi` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `provinsi`
--

INSERT INTO `provinsi` (`idprovinsi`, `provinsi`) VALUES
(1, 'Jawa Timur'),
(2, 'Jawa Tengah'),
(3, 'Jawa Barat');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rtrw`
--

CREATE TABLE `rtrw` (
  `idrtrw` int(11) NOT NULL,
  `rtrw` varchar(255) DEFAULT NULL,
  `iddesa` int(11) DEFAULT NULL,
  `info_tambahan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `rtrw`
--

INSERT INTO `rtrw` (`idrtrw`, `rtrw`, `iddesa`, `info_tambahan`) VALUES
(1, '09/03', 1, 'N/A');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `iduser` int(11) NOT NULL,
  `nik` varchar(128) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `username` varchar(128) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `notelp` varchar(32) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `idrole` int(11) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`iduser`, `nik`, `nama`, `username`, `email`, `notelp`, `password`, `idrole`, `date_created`) VALUES
(1, NULL, 'Admin Desa', 'admin', 'admin@gmail.com', '08123', '$2y$10$hbEtfnIU5y4cgbc15m7jQevyafL9iGJcbVgDsdo2dVhaRzhkQwaeC', 2, '2020-04-19 09:36:22'),
(2, '1000123', 'Budi Sudarsono', 'budisudarsono', 'budisudarsono@gmail.com', '089123', '$2y$10$hbEtfnIU5y4cgbc15m7jQevyafL9iGJcbVgDsdo2dVhaRzhkQwaeC', 3, '2020-04-19 09:37:31'),
(4, '1000122', 'Mahendra Zhafir', 'hendra', 'hendra@gmail.com', '08124723', '$2y$10$WJ4jbdZw/Ui2HipjzseSge6IjVZ/72WyYasJ/zLdd.aQ4ng7qme6m', 3, '2020-04-23 09:26:40');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_role`
--

CREATE TABLE `user_role` (
  `idrole` int(11) NOT NULL,
  `role` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_role`
--

INSERT INTO `user_role` (`idrole`, `role`) VALUES
(1, 'Kepala Desa'),
(2, 'Admin'),
(3, 'Penduduk');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `bukti`
--
ALTER TABLE `bukti`
  ADD PRIMARY KEY (`idbukti`),
  ADD KEY `nik` (`penduduk_meninggal`);

--
-- Indeks untuk tabel `cetaksurat`
--
ALTER TABLE `cetaksurat`
  ADD PRIMARY KEY (`idcetak`),
  ADD KEY `iduser` (`nik`),
  ADD KEY `idjenissurat` (`idjenissurat`),
  ADD KEY `cetak_by` (`cetak_by`),
  ADD KEY `idbukti` (`idbukti`);

--
-- Indeks untuk tabel `desa`
--
ALTER TABLE `desa`
  ADD PRIMARY KEY (`iddesa`),
  ADD KEY `idkecamatan` (`idkecamatan`),
  ADD KEY `idkabupaten` (`idkabupaten`),
  ADD KEY `idprovinsi` (`idprovinsi`);

--
-- Indeks untuk tabel `jenissurat`
--
ALTER TABLE `jenissurat`
  ADD PRIMARY KEY (`idjenissurat`);

--
-- Indeks untuk tabel `kabupaten`
--
ALTER TABLE `kabupaten`
  ADD PRIMARY KEY (`idkabupaten`),
  ADD KEY `idprovinsi` (`idprovinsi`);

--
-- Indeks untuk tabel `kartukeluarga`
--
ALTER TABLE `kartukeluarga`
  ADD PRIMARY KEY (`nokk`),
  ADD UNIQUE KEY `nokk` (`nokk`),
  ADD KEY `iddesa` (`iddesa`),
  ADD KEY `rtrw` (`idrtrw`);

--
-- Indeks untuk tabel `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`idkecamatan`),
  ADD KEY `idkabupaten` (`idkabupaten`),
  ADD KEY `idprovinsi` (`idprovinsi`);

--
-- Indeks untuk tabel `list_keluarga`
--
ALTER TABLE `list_keluarga`
  ADD PRIMARY KEY (`idlist`),
  ADD KEY `nokk` (`nokk`),
  ADD KEY `nik` (`nik`);

--
-- Indeks untuk tabel `penduduk`
--
ALTER TABLE `penduduk`
  ADD PRIMARY KEY (`nik`);

--
-- Indeks untuk tabel `pengaturan`
--
ALTER TABLE `pengaturan`
  ADD PRIMARY KEY (`idpengaturan`);

--
-- Indeks untuk tabel `pesan`
--
ALTER TABLE `pesan`
  ADD PRIMARY KEY (`idpesan`);

--
-- Indeks untuk tabel `provinsi`
--
ALTER TABLE `provinsi`
  ADD PRIMARY KEY (`idprovinsi`);

--
-- Indeks untuk tabel `rtrw`
--
ALTER TABLE `rtrw`
  ADD PRIMARY KEY (`idrtrw`),
  ADD KEY `iddesa` (`iddesa`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`iduser`),
  ADD KEY `idrole` (`idrole`),
  ADD KEY `nik` (`nik`);

--
-- Indeks untuk tabel `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`idrole`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `desa`
--
ALTER TABLE `desa`
  MODIFY `iddesa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `jenissurat`
--
ALTER TABLE `jenissurat`
  MODIFY `idjenissurat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `kabupaten`
--
ALTER TABLE `kabupaten`
  MODIFY `idkabupaten` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `kecamatan`
--
ALTER TABLE `kecamatan`
  MODIFY `idkecamatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `list_keluarga`
--
ALTER TABLE `list_keluarga`
  MODIFY `idlist` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `pesan`
--
ALTER TABLE `pesan`
  MODIFY `idpesan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `provinsi`
--
ALTER TABLE `provinsi`
  MODIFY `idprovinsi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `rtrw`
--
ALTER TABLE `rtrw`
  MODIFY `idrtrw` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `user_role`
--
ALTER TABLE `user_role`
  MODIFY `idrole` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `bukti`
--
ALTER TABLE `bukti`
  ADD CONSTRAINT `bukti_ibfk_1` FOREIGN KEY (`penduduk_meninggal`) REFERENCES `penduduk` (`nik`);

--
-- Ketidakleluasaan untuk tabel `cetaksurat`
--
ALTER TABLE `cetaksurat`
  ADD CONSTRAINT `cetaksurat_ibfk_2` FOREIGN KEY (`cetak_by`) REFERENCES `users` (`iduser`),
  ADD CONSTRAINT `cetaksurat_ibfk_3` FOREIGN KEY (`idjenissurat`) REFERENCES `jenissurat` (`idjenissurat`),
  ADD CONSTRAINT `cetaksurat_ibfk_4` FOREIGN KEY (`idbukti`) REFERENCES `bukti` (`idbukti`),
  ADD CONSTRAINT `cetaksurat_ibfk_5` FOREIGN KEY (`nik`) REFERENCES `penduduk` (`nik`);

--
-- Ketidakleluasaan untuk tabel `desa`
--
ALTER TABLE `desa`
  ADD CONSTRAINT `desa_ibfk_1` FOREIGN KEY (`idkabupaten`) REFERENCES `kabupaten` (`idkabupaten`),
  ADD CONSTRAINT `desa_ibfk_2` FOREIGN KEY (`idkecamatan`) REFERENCES `kecamatan` (`idkecamatan`),
  ADD CONSTRAINT `desa_ibfk_3` FOREIGN KEY (`idprovinsi`) REFERENCES `provinsi` (`idprovinsi`);

--
-- Ketidakleluasaan untuk tabel `kabupaten`
--
ALTER TABLE `kabupaten`
  ADD CONSTRAINT `kabupaten_ibfk_1` FOREIGN KEY (`idprovinsi`) REFERENCES `provinsi` (`idprovinsi`);

--
-- Ketidakleluasaan untuk tabel `kartukeluarga`
--
ALTER TABLE `kartukeluarga`
  ADD CONSTRAINT `kartukeluarga_ibfk_1` FOREIGN KEY (`iddesa`) REFERENCES `desa` (`iddesa`),
  ADD CONSTRAINT `kartukeluarga_ibfk_2` FOREIGN KEY (`idrtrw`) REFERENCES `rtrw` (`idrtrw`);

--
-- Ketidakleluasaan untuk tabel `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD CONSTRAINT `kecamatan_ibfk_1` FOREIGN KEY (`idkabupaten`) REFERENCES `kabupaten` (`idkabupaten`),
  ADD CONSTRAINT `kecamatan_ibfk_2` FOREIGN KEY (`idprovinsi`) REFERENCES `provinsi` (`idprovinsi`);

--
-- Ketidakleluasaan untuk tabel `list_keluarga`
--
ALTER TABLE `list_keluarga`
  ADD CONSTRAINT `list_keluarga_ibfk_1` FOREIGN KEY (`nik`) REFERENCES `penduduk` (`nik`),
  ADD CONSTRAINT `list_keluarga_ibfk_2` FOREIGN KEY (`nokk`) REFERENCES `kartukeluarga` (`nokk`);

--
-- Ketidakleluasaan untuk tabel `rtrw`
--
ALTER TABLE `rtrw`
  ADD CONSTRAINT `rtrw_ibfk_1` FOREIGN KEY (`iddesa`) REFERENCES `desa` (`iddesa`);

--
-- Ketidakleluasaan untuk tabel `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`idrole`) REFERENCES `user_role` (`idrole`),
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`nik`) REFERENCES `penduduk` (`nik`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
